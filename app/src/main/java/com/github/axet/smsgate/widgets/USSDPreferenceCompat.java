package com.github.axet.smsgate.widgets;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.services.USSDService;

public class USSDPreferenceCompat extends SwitchPreferenceCompat {
    public static final String TAG = USSDPreferenceCompat.class.getSimpleName();

    public Activity a;

    public static boolean isEnabled(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        String packageName = USSDService.class.getCanonicalName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    public static void show(Context context) {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        context.startActivity(intent);
    }

    @TargetApi(21)
    public USSDPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public USSDPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public USSDPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public USSDPreferenceCompat(Context context) {
        super(context);
        create();
    }

    void create() {
        onResume();
    }

    @Override
    protected void onClick() {
        show(getContext());
    }

    public void onResume() {
        setChecked(isEnabled(getContext()));
    }
}
