package com.github.axet.smsgate.services;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.github.axet.smsgate.app.SMSApplication;
import com.zegoggles.smssync.activity.SMSGateFragment;

@TargetApi(18)
public class NotificationListener extends NotificationListenerService {
    public static final String TAG = NotificationListener.class.getSimpleName();

    public static final String REFRESH = NotificationListener.class.getCanonicalName() + ".REFRESH";
    public static final String CANCEL = NotificationListener.class.getCanonicalName() + ".CANCEL";

    public static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners"; // Settings.Secure@hide

    BroadcastReceiver receiver;

    public static void refresh(Context context) {
        Intent intent = new Intent(REFRESH);
        context.sendBroadcast(intent);
    }

    public static void show(Context context) {
        context.startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
    }

    public static String notificationCode(String pkg, String tag, int id) {
        return SMSApplication.toHexString((pkg + "_" + tag + "_" + id).hashCode());
    }

    public static boolean enabled(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, ENABLED_NOTIFICATION_LISTENERS);
        String packageName = NotificationListener.class.getCanonicalName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    public static void startIfEnabled(Context context) {
        if (enabled(context))
            refresh(context);
    }

    public static void cancel(Context context, String id) {
        Intent intent = new Intent(NotificationListener.CANCEL);
        intent.putExtra("id", id);
        context.sendBroadcast(intent);
    }

    public static void requestRebind(Context context) {
        if (Build.VERSION.SDK_INT >= 24)
            NotificationListenerService.requestRebind(new ComponentName(context, NotificationListener.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent == null)
                    return;
                String a = intent.getAction();
                if (a == null)
                    return;
                if (a.equals(REFRESH))
                    NotificationListener.this.refresh();
                if (a.equals(CANCEL)) {
                    String id = intent.getStringExtra("id");
                    for (StatusBarNotification n : getActiveNotifications()) {
                        String key = notificationCode(n.getPackageName(), n.getTag(), n.getId());
                        if (key.equals(id)) {
                            if (Build.VERSION.SDK_INT >= 21)
                                cancelNotification(n.getKey());
                            else
                                cancelNotification(n.getPackageName(), n.getTag(), n.getId());
                        }
                    }
                }
            }
        };
        IntentFilter ff = new IntentFilter();
        ff.addAction(REFRESH);
        ff.addAction(CANCEL);
        registerReceiver(receiver, ff);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return super.onBind(intent);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        Log.d(TAG, "onListenerConnected");
        refresh();
    }

    void refresh() {
        StatusBarNotification[] nn = getActiveNotifications();
        if (nn == null)
            return;
        for (StatusBarNotification n : nn)
            send(NotificationService.UPDATE, n);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification n) {
        super.onNotificationPosted(n);
        send(NotificationService.ADD, n);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        send(NotificationService.REMOVE, sbn);
    }

    void send(String action, StatusBarNotification n) {
        SMSApplication.from(this).notifications.notification(action, n.getPackageName(), notificationCode(n.getPackageName(), n.getTag(), n.getId()), n.getNotification());
        SMSGateFragment.checkPermissions(this);
        FirebaseService.notification(this, action, n.getPackageName(), notificationCode(n.getPackageName(), n.getTag(), n.getId()), n.getNotification());
    }
}
