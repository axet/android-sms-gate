package com.github.axet.smsgate.app;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMessageHelper;
import com.fsck.k9.mail.internet.TextBody;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.NotificationService;
import com.github.axet.smsgate.widgets.IMAPSyncPreferenceCompat;
import com.github.axet.smsgate.widgets.NotificationsPreference;
import com.zegoggles.smssync.mail.Headers;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.service.ImapSmsService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class NotificationsStorage {
    public static String TAG = NotificationsStorage.class.getSimpleName();

    public static int NOTIFICATION_UPDATE_RATE = 60 * 1000;

    Context context;
    Storage storage;
    Handler handler = new Handler();
    NotificationsMap<NotificationPkgInfo> lastId = new NotificationsMap<>(handler);

    public static String getFormatted(long sent, String pkg, String from, String ff) {
        Storage.Entry sms = new Storage.CallEntry();
        sms.sent = sent;
        sms.type = "";
        sms.threadPhone = pkg;
        sms.threadName = from;
        SIM sim = new SIM();
        return Storage.getFormatted(sim, ff, sms);
    }

    public static String formatBody(String from, long sent, String s, String b) {
        String body = "(" + from + ") " + Storage.SIMPLE.format(sent);
        body += " " + s;
        body += "\n\n";
        body += b;
        body += "\n";
        return body;
    }

    public static TagMimeMessage formatMessage(Context context, NotificationText o, String userEmail) {
        final TagMimeMessage msg = new TagMimeMessage();
        try {
            String from = FirebaseService.getApplicationName(context, o.pkg);
            msg.tag = o;
            msg.setFrom(new Address(userEmail, from));
            msg.setRecipient(Message.RecipientType.TO, new Address(userEmail));
            msg.setSubject(o.title);
            msg.setHeader(Headers.REFERENCES, String.format("<%d.%d@sms-backup-plus.local>", from.hashCode(), o.title.hashCode()));
            MimeMessageHelper.setBody(msg, new TextBody(o.details));
            return msg;
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public static class NotificationText {
        public long timestamp;
        public String pkg;
        public int code;
        public String title;
        public String text;
        public String details;

        public NotificationText(String json) {
            load(json);
        }

        public NotificationText(String pkg, Notification n, long now) {
            this.pkg = pkg;
            if (Build.VERSION.SDK_INT >= 19) {
                title = FirebaseService.toString(n.extras.get("android.title"));
                text = FirebaseService.toString(n.extras.get("android.text")); // can be spannable
                if (text == null || text.isEmpty())
                    text = FirebaseService.toString(n.tickerText); // can be null
                details = FirebaseService.toString(n.extras.get("android.bigText")); // class android.text.SpannableString
            } else {
                text = FirebaseService.toString(n.tickerText); // can be null
            }

            if (title == null || title.isEmpty()) {
                title = text;
                if (title == null || title.isEmpty())
                    title = details;
                if (title == null || title.isEmpty())
                    title = "";
                if (details == null || details.isEmpty())
                    details = text;
                if (details == null || details.isEmpty())
                    details = "";
            } else if (details == null || details.isEmpty()) {
                details = text;
                if (details == null || details.isEmpty())
                    details = "";
            }

            timestamp = now;
            update();
        }

        public void update() {
            code = (title + " " + details).hashCode();
        }

        public JSONObject save() throws JSONException {
            JSONObject json = new JSONObject();
            json.put("timestamp", timestamp);
            json.put("pkg", pkg);
            json.put("subject", title);
            json.put("text", text);
            json.put("body", details);
            return json;
        }

        public void load(String json) {
            try {
                JSONObject o = new JSONObject(json);
                load(o);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public void load(JSONObject o) throws JSONException {
            timestamp = o.getLong("timestamp");
            pkg = o.getString("pkg");
            title = o.getString("subject");
            text = o.getString("text");
            details = o.getString("body");
            update();
        }

        public boolean equals(NotificationText text) {
            return pkg.equals(text.pkg) && title.equals(text.title) && details.equals(text.details);
        }
    }

    public static class NotificationInfo {
        public String id;
        public int code;
        public Runnable delay;
        public long last;

        public NotificationInfo(String id) {
            this.id = id;
        }
    }

    public static class NotificationPkgInfo extends NotificationInfo {
        String pkg;
        String nid;
        Notification n;

        public NotificationPkgInfo(String id) {
            super(id);
        }
    }

    public static class NotificationsMap<T extends NotificationInfo> extends HashMap<String, T> {
        Handler handler;

        public NotificationsMap(Handler handler) {
            this.handler = handler;
        }

        public boolean duplicate(NotificationInfo cc, int code) {
            if (cc != null) { // already exits
                if (cc.code == code) // do not update same notification with same text twice
                    return true;
            }
            return false;
        }

        public boolean delayed(NotificationInfo cc, long now) {
            if (now - cc.last < NOTIFICATION_UPDATE_RATE)
                return true;
            return false;
        }

        public void delay(NotificationInfo cc, Runnable run) {
            if (cc.delay == null) {
                cc.delay = run;
                handler.postDelayed(cc.delay, NOTIFICATION_UPDATE_RATE);
            }
        }

        public void remove(String id) {
            T ni = super.remove(id);
            if (ni != null) {
                if (ni.delay != null)
                    handler.removeCallbacks(ni.delay);
            }
        }

        public void put(T cc, int code, long now) {
            cc.code = code;
            cc.last = now;
            if (cc.delay != null) {
                handler.removeCallbacks(cc.delay);
                cc.delay = null;
            }
            super.put(cc.id, cc);
        }
    }

    public NotificationsStorage(Context context) {
        this.context = context;
        this.storage = new Storage(context);
    }

    public void notification(String action, String pkg, String id, Notification n) {
        if (Build.VERSION.SDK_INT >= 16) {
            if (n.priority <= Notification.PRIORITY_LOW)
                return; // ignore low priority notifications
        }
        if (action.equals(NotificationService.REMOVE)) {
            lastId.remove(pkg);
            return;
        }
        add(pkg, n);
    }

    public void add(String pkg, Notification n) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> ss = NotificationsPreference.load(prefs.getString(SMSApplication.PREF_APPS, ""));
        if (!NotificationsPreference.contains(ss, pkg))
            return;

        long now = System.currentTimeMillis();

        NotificationText text = new NotificationText(pkg, n, now);

        if (Build.VERSION.SDK_INT >= 18) { // old api has only UPDATE events and no REMOVE, add them all
            NotificationPkgInfo last = lastId.get(pkg);
            if (last == null)
                last = new NotificationPkgInfo(pkg);

            if (lastId.duplicate(last, text.code))
                return; // do not email same text twice

            last.pkg = pkg;
            last.n = n;

            if (lastId.delayed(last, now)) {
                final NotificationPkgInfo info = last;
                lastId.delay(last, new Runnable() {
                    @Override
                    public void run() {
                        add(info.pkg, info.n);
                    }
                });
                return;
            }

            lastId.put(last, text.code, now);
        }

        add(text);
    }

    public void add(NotificationText text) {
        writeFile(text);

        if (!IMAPSyncPreferenceCompat.isEnabled(context))
            return;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        int i = prefs.getInt(SMSApplication.APPS_COUNT, 0);

        for (int j = 0; j < i; j++) {
            String json = prefs.getString(SMSApplication.APPS_INDEX + j, "");
            NotificationText t = new NotificationText(json);
            if (t.equals(text))
                return; // duplicate
        }

        int count = i + 1;

        try {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(SMSApplication.APPS_INDEX + i, text.save().toString());
            edit.putInt(SMSApplication.APPS_COUNT, count);
            edit.apply();
        } catch (JSONException e) {
            Log.d(TAG, "unable to process message", e); // ignore
        }

        ImapSmsService.scheduleBackup(context, 3); // 3 sec delay
    }

    public NotificationText getMessage(int i) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(SMSApplication.APPS_INDEX + i, "");
        return new NotificationText(json);
    }

    public void delete(int i) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int count = prefs.getInt(SMSApplication.APPS_COUNT, 0);
        SharedPreferences.Editor edit = prefs.edit();
        int last = count - 1;
        for (int k = i; k < last; k++) {
            int next = k + 1;
            String json = prefs.getString(SMSApplication.APPS_INDEX + next, "");
            edit.putString(SMSApplication.APPS_INDEX + k, json);
        }
        edit.putInt(SMSApplication.APPS_COUNT, last);
        edit.commit();
    }

    public int getCount() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(SMSApplication.APPS_COUNT, 0);
    }

    public void writeFile(NotificationText o) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            String from = FirebaseService.getApplicationName(context, o.pkg);
            String ff = shared.getString(SMSApplication.PREF_NAMEFORMAT, SMSApplication.PREF_NAMEFORMAT_DEFAULT);
            String name = getFormatted(o.timestamp, o.pkg, from, ff);
            String text = formatBody(from, o.timestamp, o.title, o.details);
            storage.writeFile(name, text);
        } catch (IOException e) {
            Log.e(TAG, "unable to write file", e);
            Toast.Error(context, "unable to write file", e);
        }
    }

}
