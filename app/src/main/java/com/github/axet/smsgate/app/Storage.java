package com.github.axet.smsgate.app;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Telephony;
import android.support.v7.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.crypto.MD5;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.providers.SMS;
import com.zegoggles.smssync.mail.PersonLookup;
import com.zegoggles.smssync.mail.PersonRecord;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class Storage extends com.github.axet.androidlibrary.app.Storage {
    public static final String TAG = Storage.class.getSimpleName();

    public static final String IN = "IN";
    public static final String OUT = "OUT";
    public static final String TXT = "txt";

    public static final SimpleDateFormat YEAR = new SimpleDateFormat("yyyy");
    public static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TIME = new SimpleDateFormat("HH.mm.ss");
    public static final SimpleDateFormat SIMPLE = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
    public static final SimpleDateFormat ISO8601 = new SimpleDateFormat("yyyyMMdd\'T\'HHmmss");

    public static String FORMAT_SIM = "%S";

    PersonLookup mPersonLookup;
    Handler handler = new Handler();
    SmsStorage storage;
    CallsStorage callsStorage;

    public static boolean isEnabled(Context context) {
        final Storage storage = new Storage(context);
        Uri uri = storage.getStoragePath();
        return uri != null;
    }

    public static long getLastSyncDate(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getLong(SMSApplication.STORAGE_SMS_LAST, 0);
    }

    public static void setLastSyncDate(Context context, long date) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(SMSApplication.STORAGE_SMS_LAST, date);
        edit.commit();
    }

    public static long getLastReplyDate(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getLong(SMSApplication.STORAGE_SMS_LAST_REPLY, 0);
    }

    public static void setLastReplyDate(Context context, long date) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(SMSApplication.STORAGE_SMS_LAST_REPLY, date);
        edit.commit();
    }

    public static long getLastCallsDate(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getLong(SMSApplication.STORAGE_CALLS_LAST, 0);
    }

    public static void setLastCallsDate(Context context, long date) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(SMSApplication.STORAGE_CALLS_LAST, date);
        edit.commit();
    }

    public static boolean filter(List<ScheduleSMS> items, SmsStorage.Message m) { // true - to ignore message
        for (ScheduleSMS item : items) {
            if (item.hide) {
                String bm = m.body.trim();
                String bi = item.message.trim();
                if (PhoneNumberUtils.compare(item.phone, m.phone) && bm.equals(bi))
                    return true;
            }
        }
        return false;
    }

    public static boolean filter(Context context, SmsStorage.Message m) { // filter received message (by contacts or hiden checkbox) true - filter message
        SharedPreferences shard = PreferenceManager.getDefaultSharedPreferences(context);
        if (shard.getBoolean(SMSApplication.PREF_CONTACTS_ONLY, false)) {
            PersonLookup lookup = new PersonLookup(context.getContentResolver()); // dynamic, no cache
            PersonRecord p = lookup.lookupPersonNew(m.phone);
            if (p.isUnknown())
                return true;
        }
        if (filter(SMSApplication.from(context).schedules, m))
            return true;
        return false; // not removing
    }

    public static SMSMessage messageFromMap(PersonLookup contacts, Map<String, String> msgMap) {
        SMSMessage sms = new SMSMessage();

        sms.id = msgMap.get(SmsStorage.ID);
        sms.message = msgMap.get(Telephony.TextBasedSmsColumns.BODY);

        final int messageType = toInt(msgMap.get(Telephony.TextBasedSmsColumns.TYPE));

        sms.thread = msgMap.get(Telephony.TextBasedSmsColumns.THREAD_ID);

        final String address = msgMap.get(Telephony.TextBasedSmsColumns.ADDRESS);
        if (TextUtils.isEmpty(address)) {
            sms.threadPhone = "--";
            sms.threadName = "--";
        } else {
            PersonRecord record = contacts.lookupPersonNew(address);
            sms.threadPhone = record.getNumber();
            sms.threadName = record.getName();
        }

        if (sms.thread == null || sms.thread.isEmpty())
            sms.thread = sms.threadPhone;

        if (SmsStorage.IN == messageType) // Received message
            sms.type = IN;
        else // Sent message
            sms.type = OUT;

        long sentDate;
        try {
            sentDate = Long.valueOf(msgMap.get(Telephony.TextBasedSmsColumns.DATE));
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = System.currentTimeMillis();
        }
        sms.sent = sentDate;

        return sms;
    }

    public static SMSMessage getMessage(SIM sim, PersonLookup contacts, SmsStorage.Message m) {
        SMSMessage sms = new SMSMessage();

        sms.id = String.valueOf(m.id);
        sms.message = m.body;

        final int messageType = m.type;

        sms.thread = String.valueOf(m.thread);

        final String address = m.phone;
        if (TextUtils.isEmpty(address)) {
            sms.threadPhone = "--";
            sms.threadName = "--";
        } else {
            PersonRecord record = contacts.lookupPersonNew(address);
            sms.threadPhone = record.getNumber();
            sms.threadName = record.getName();
        }

        if (sms.thread == null || sms.thread.isEmpty() || sms.thread.equals("0") || sms.thread.equals("-1"))
            sms.thread = sms.threadPhone;

        if (SmsStorage.IN == messageType) // Received message
            sms.type = IN;
        else // Sent message
            sms.type = OUT;

        sms.sent = m.date;
        sms.simSlot = sim.findID(m.simID);

        return sms;
    }

    public static CallEntry getMessage(SIM sim, PersonLookup contacts, CallsStorage.Call m) {
        CallEntry entry = new CallEntry();

        entry.id = String.valueOf(m.id);

        final int messageType = m.type;

        final String address = m.phone;
        if (TextUtils.isEmpty(address)) {
            entry.threadPhone = "--";
            entry.threadName = "--";
        } else {
            PersonRecord record = contacts.lookupPersonNew(address);
            entry.threadPhone = record.getNumber();
            entry.threadName = record.getName();
        }

        if (CallsStorage.IN == messageType) // Received message
            entry.type = IN;
        else // Sent message
            entry.type = OUT;

        entry.sent = m.date;
        entry.duration = m.duration;
        entry.missed = m.missed;
        entry.simSlot = sim.findID(m.simID);

        return entry;
    }

    public static String filterSymbols(String name) { // filter symbols not allowed on vfat
        name = name.replaceAll("\\*", "∗");
        name = name.replaceAll("\\?", "？");
        name = name.replaceAll("\\|", "｜");
        name = name.replaceAll("\\\\", "＼");
        name = name.replaceAll("/", "／");
        name = name.replaceAll("\"", "＂");
        name = name.replaceAll(":", "：");
        name = name.replaceAll(">", "＞");
        name = name.replaceAll("<", "＜");
        return name;
    }

    public static String formatName(SMSMessage sms) {
        String name = ISO8601.format(sms.sent);
        if (!sms.type.isEmpty())
            name += " " + (sms.type.equals(IN) ? "↓" : "↑");
        name += " " + PhoneNumberUtils.formatNumber(sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            name += " (" + sms.threadName + ")";
        name += ".txt";
        return filterSymbols(name);
    }

    public static String formatBody(SIM sim, String from, SMSMessage sms) {
        String body = "(" + from + ") " + SIMPLE.format(sms.sent);
        if (!sms.type.isEmpty())
            body += " " + (sms.type.equals(IN) ? "↓" : "↑");
        if (sim.getCount() > 1)
            body += " " + "SIM" + sms.simSlot;
        body += " " + PhoneNumberUtils.formatNumber(sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            body += " (" + sms.threadName + ")";
        body += "\n\n";
        body += sms.message;
        body += "\n";
        return body;
    }

    public static String formatBody(SIM sim, String from, CallEntry call) {
        String body = "(" + from + ") " + SIMPLE.format(call.sent);
        if (!call.type.isEmpty())
            body += " " + (call.type.equals(IN) ? "↓" : "↑");
        if (sim.getCount() > 1)
            body += " " + "SIM" + call.simSlot;
        body += " " + PhoneNumberUtils.formatNumber(call.threadPhone);
        if (!call.threadName.isEmpty() && !call.threadPhone.equals(call.threadName))
            body += " (" + call.threadName + ")";
        body += "\n\n";
        if (call.missed)
            body += sim.context.getString(R.string.call_missed);
        else
            body += MainApplication.formatDuration(sim.context, call.duration, false, true, false);
        body += "\n";
        return body;
    }

    public static int toInt(String s) {
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static long toLong(String s) {
        try {
            return Long.valueOf(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String getNameFormatted(SIM sim, String f, Entry sms) {
        f = f.replaceAll("%Y", YEAR.format(sms.sent));
        f = f.replaceAll("%D", SIMPLE.format(sms.sent));
        f = f.replaceAll("%d", ISO8601.format(sms.sent));
        if (sms.type.isEmpty())
            f = f.replaceAll("%t", "");
        else
            f = f.replaceAll("%t", sms.type.equals(IN) ? "↓" : "↑");
        if (sim.getCount() > 1 && sms.simSlot >= 0)
            f = f.replaceAll(FORMAT_SIM, "SIM" + sms.simSlot);
        else
            f = f.replaceAll(FORMAT_SIM, "");
        f = f.replaceAll("%P", PhoneNumberUtils.formatNumber(sms.threadPhone));
        f = f.replaceAll("%p", sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            f = f.replaceAll("%n", sms.threadName);
        else
            f = f.replaceAll("%n", "");
        f = f.replaceAll("%m", Build.MODEL);
        f = f.replaceAll("\\(\\)", "");
        f = f.replaceAll(" +", " ");
        f = f.trim();
        return f;
    }

    public static String getFormatted(SIM sim, String f, Entry sms) {
        return getNameFormatted(sim, f, sms) + "." + TXT;
    }

    public static List<Node> filter(List<Node> nn, NodeFilter f) {
        ArrayList<Node> rr = new ArrayList<>();
        for (Node n : nn) {
            if (f.accept(n))
                rr.add(n);
        }
        return rr;
    }

    public static class Entry {
        public String id;
        public long sent; // sent date
        public String type; // IN/OUT
        public String threadPhone; // phone
        public String threadName; // person Full Name
        public int simSlot = SubscriptionManager.INVALID_SIM_SLOT_INDEX;

        public Entry() {
        }

        public Entry(String json) {
            fromJson(json);
        }

        public Entry(long s, String type, String phone, String name, int simSlot) {
            this.sent = s;
            this.type = type;
            this.threadPhone = phone;
            this.threadName = name;
            this.simSlot = simSlot;
        }

        public void toJSON(JSONObject j) {
            try {
                j.put("id", id);
                j.put("sent", sent);
                j.put("type", type);
                j.put("threadPhone", threadPhone);
                j.put("threadName", threadName);
                j.put("simSlot", simSlot);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public String toJSON() {
            JSONObject j = new JSONObject();
            toJSON(j);
            return j.toString();
        }

        public void fromJson(String json) {
            try {
                JSONObject f = new JSONObject(json);
                fromJson(f);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public void fromJson(JSONObject f) throws JSONException {
            id = f.getString("id");
            sent = f.getLong("sent");
            type = f.getString("type");
            threadPhone = f.getString("threadPhone");
            threadName = f.getString("threadName");
            simSlot = f.getInt("simSlot");
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public long getSent() {
            return sent;
        }

        public void setSent(long sent) {
            this.sent = sent;
        }

        public String getThreadName() {
            return threadName;
        }

        public String getThreadPhone() {
            return threadPhone;
        }

        public void setThreadName(String threadName) {
            this.threadName = threadName;
        }

        public void setThreadPhone(String threadPhone) {
            this.threadPhone = threadPhone;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public int getSimSlot() {
            return simSlot;
        }

        public void setSimSlot(int simSlot) {
            this.simSlot = simSlot;
        }
    }

    public static class SMSMessage extends Entry {
        public String thread; // thread id
        public String message; // body

        public SMSMessage() {
        }

        public SMSMessage(String json) {
            fromJson(json);
        }

        public SMSMessage(long s, String type, String phone, String name, String msg, int simSlot) {
            super(s, type, phone, name, simSlot);
            this.message = msg;
        }

        public String toJSON() {
            try {
                JSONObject j = new JSONObject();
                super.toJSON(j);
                j.put("thread", thread);
                j.put("message", message);
                return j.toString();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public void fromJson(String json) {
            try {
                JSONObject f = new JSONObject(json);
                super.fromJson(f);
                thread = f.getString("thread");
                message = f.getString("message");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public String getThread() {
            return thread;
        }

        public void setThread(String thread) {
            this.thread = thread;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class CallEntry extends Entry {
        public long duration;
        public boolean missed;
    }

    public Storage(Context context) {
        super(context);
        mPersonLookup = new PersonLookup(context.getContentResolver());
        storage = new SmsStorage(context);
        callsStorage = new CallsStorage(context);
    }

    public void incoming(final long last, final Runnable done) {
        final Runnable run = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                count++;
                if (count > 10) {
                    if (done != null)
                        done.run();
                    return;
                }
                if (messages() < last) {  // we expect message. not yet here? retry
                    handler.postDelayed(this, 1000);
                } else {
                    if (done != null)
                        done.run();
                }
            }
        };
        run.run();
    }

    public long messages() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        Cursor cursor = storage.query(getLastSyncDate(context), Telephony.TextBasedSmsColumns.DATE, -1, -1);
        String from = Build.DEVICE;
        SIM sim = SMSApplication.from(context).getSIM();

        long last = 0;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                SmsStorage.Message m = SmsStorage.getMessage(cursor);
                if (Storage.filter(context, m)) {
                    last = m.remote_date;
                    setLastSyncDate(context, m.date);
                    continue;
                }
                SMSMessage sms = getMessage(sim, mPersonLookup, m);
                String ff = shared.getString(SMSApplication.PREF_NAMEFORMAT, SMSApplication.PREF_NAMEFORMAT_DEFAULT);
                String name = getFormatted(sim, ff, sms);
                String text = formatBody(sim, from, sms);
                try {
                    writeFile(name, text);
                } catch (IOException e) {
                    Log.e(TAG, "unable to write file", e);
                    Toast.Error(context, "unable to write file", e);
                    return 0;
                }
                last = m.remote_date;
                setLastSyncDate(context, m.date);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
            cursor.close();
        }

        return last;
    }

    void writeFile(String name, String text) throws IOException {
        Uri uri = getStoragePath();
        String s = uri.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            File file = getFile(uri);
            File f = new File(file, name);
            Storage.mkdirs(f.getParentFile());
            FileWriter out = new FileWriter(f);
            IOUtils.write(text, out);
            out.close();
        } else if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT)) {
            Uri file = createFile(context, uri, name);
            ContentResolver resolver = context.getContentResolver();
            OutputStream os = resolver.openOutputStream(file);
            OutputStreamWriter out = new OutputStreamWriter(os);
            IOUtils.write(text, out);
            out.close();
        } else {
            throw new UnknownUri();
        }
    }

    public long calls() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        Cursor cursor = callsStorage.query(getLastCallsDate(context), Telephony.TextBasedSmsColumns.DATE, -1, -1);
        String from = Build.DEVICE;
        SIM sim = SMSApplication.from(context).getSIM();

        long last = 0;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                CallsStorage.Call m = CallsStorage.getCall(cursor);
                CallEntry call = getMessage(sim, mPersonLookup, m);
                String ff = shared.getString(SMSApplication.PREF_NAMEFORMAT, SMSApplication.PREF_NAMEFORMAT_DEFAULT);
                String name = getFormatted(sim, ff, call);
                String text = formatBody(sim, from, call);
                try {
                    writeFile(name, text);
                } catch (IOException e) {
                    Log.e(TAG, "unable to write file", e);
                    Toast.Error(context, "unable to write file", e);
                    return 0;
                }
                last = m.date;
                setLastCallsDate(context, m.date);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
            cursor.close();
        }

        return last;
    }

    public Uri getStoragePath() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String path = sharedPreferences.getString(SMSApplication.PREF_STORAGE, "");
        if (path.isEmpty())
            return null;
        return super.getStoragePath(path);
    }

    @Override
    public void migrateLocalStorage() {
        migrateLocalStorage(getLocalInternal());
        migrateLocalStorage(getLocalExternal());
    }

    public void migrateLocalStorage(File file) {
        if (file == null)
            return;
        Uri uri = getStoragePath();
        String s = uri.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE) && getFile(uri).equals(file))
            return;
        File[] ff = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return getExt(pathname).toLowerCase().equals(TXT);
            }
        });
        if (ff == null)
            return;
        for (File f : ff) {
            Uri u = migrate(context, f, uri);
            if (u == null)
                throw new PermissionDenied(f.getName());
        }
    }

    public void reply() { // allow reply using file system "file.txt -> file suffix.txt"
        migrateLocalStorage();

        final AtomicLong lastReply = new AtomicLong(getLastReplyDate(context));
        Uri uri = getStoragePath();
        ArrayList<Node> nn = Storage.list(context, uri, new NodeFilter() { // list recent files
            final long date = lastReply.get();

            @Override
            public boolean accept(Node n) {
                if (n.last > lastReply.get())
                    lastReply.set(n.last);
                return date < n.last;
            }
        });

        if (nn.size() == 0)
            return;

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SIM sim = SMSApplication.from(context).getSIM();

        Cursor cursor = storage.query(0, Telephony.TextBasedSmsColumns.DATE, -1, -1);
        if (cursor == null)
            return;
        while (cursor.moveToNext()) {
            SmsStorage.Message m = SmsStorage.getMessage(cursor);
            if (Storage.filter(context, m))
                continue;
            SMSMessage sms = getMessage(sim, mPersonLookup, m);
            int simID = sim.getSimID(sms.simSlot);
            String nf = shared.getString(SMSApplication.PREF_NAMEFORMAT, SMSApplication.PREF_NAMEFORMAT_DEFAULT);
            String name = getFormatted(sim, nf, sms); // file name
            String s = uri.getScheme();
            if (s.equals(ContentResolver.SCHEME_FILE)) {
                File file = getFile(uri);
                final File f = new File(file, name);
                final long date = f.lastModified();
                if (f.exists()) { // if message exists
                    final String left = Storage.getNameNoExt(name);
                    List<Node> rr = Storage.filter(nn, new NodeFilter() {
                        @Override
                        public boolean accept(Node pathname) {
                            return pathname.name.startsWith(left) && pathname.last > date; // reply to all "file suffix.txt" and date >= original sms
                        }
                    });
                    if (rr != null && rr.size() > 0) {
                        try {
                            String md5 = MD5.digest(IOUtils.toString(new FileInputStream(f), Charset.defaultCharset()));
                            for (Node r : rr) {
                                File rf = Storage.getFile(r.uri);
                                String body = IOUtils.toString(new FileInputStream(rf), Charset.defaultCharset());
                                if (body.length() > 0 && !md5.equals(MD5.digest(body))) { // content changed, then we are ready to send it
                                    if (sim.getCount() > 1 && simID != -1)
                                        SMS.send(context, simID, m.phone, body);
                                    else
                                        SMS.send(context, m.phone, body);
                                    // Storage.touch(f);
                                    Storage.delete(rf); // touch not working on every android
                                }
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "unable to read", e);
                        }
                    }
                }
            } else if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT)) {
                Uri child = Storage.getDocumentChild(context, uri, name);
                if (Storage.exists(context, child)) {
                    final String left = Storage.getNameNoExt(name);
                    final long date = Storage.getLastModified(context, child);
                    List<Node> rr = Storage.filter(nn, new NodeFilter() {
                        @Override
                        public boolean accept(Node n) {
                            return n.name.startsWith(left) && n.last > date;
                        }
                    });
                    if (rr.size() > 0) {
                        ContentResolver resolver = context.getContentResolver();
                        try {
                            String md5 = MD5.digest(IOUtils.toString(resolver.openInputStream(child), Charset.defaultCharset()));
                            for (Node n : rr) {
                                String body = IOUtils.toString(resolver.openInputStream(n.uri), Charset.defaultCharset());
                                if (body.length() > 0 && !md5.equals(MD5.digest(body))) { // content changed, then we are ready to send it
                                    if (sim.getCount() > 1 && simID != -1)
                                        SMS.send(context, simID, m.phone, body);
                                    else
                                        SMS.send(context, m.phone, body);
                                    // Storage.touch(context, uri, name);
                                    Storage.delete(context, n.uri); // touch not working on every android
                                }
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "unable to read", e);
                        }
                    }
                }
            } else {
                throw new UnknownUri();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return; // cursor leak
            }
        }
        cursor.close();
        setLastReplyDate(context, lastReply.get());
    }
}
