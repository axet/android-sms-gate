package com.zegoggles.smssync.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.store.imap.XOAuth2AuthenticationFailedException;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.NotificationsStorage;
import com.github.axet.smsgate.app.CallsStorage;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.app.TagMimeMessage;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.services.ImapSmsReplyService;
import com.github.axet.smsgate.widgets.CallLogsPreferenceCompat;
import com.github.axet.smsgate.widgets.NotificationsPreference;
import com.github.axet.smsgate.widgets.SMSPreferenceCompat;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.auth.TokenRefreshException;
import com.zegoggles.smssync.auth.TokenRefresher;
import com.zegoggles.smssync.contacts.ContactAccessor;
import com.zegoggles.smssync.mail.BackupImapStore;
import com.zegoggles.smssync.mail.ConversionResult;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.mail.MessageConverter;
import com.zegoggles.smssync.mail.PersonLookup;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.mail.DataType.Defaults;
import static com.zegoggles.smssync.mail.DataType.MMS;
import static com.zegoggles.smssync.mail.DataType.SMS;
import static com.zegoggles.smssync.mail.DataType.CALL;
import static com.zegoggles.smssync.service.state.SmsSyncState.BACKUP;
import static com.zegoggles.smssync.service.state.SmsSyncState.CALC;
import static com.zegoggles.smssync.service.state.SmsSyncState.CANCELED_BACKUP;
import static com.zegoggles.smssync.service.state.SmsSyncState.ERROR;
import static com.zegoggles.smssync.service.state.SmsSyncState.FINISHED_BACKUP;
import static com.zegoggles.smssync.service.state.SmsSyncState.LOGIN;

class ImapBackupTask extends AsyncTask<BackupConfig, BackupState, BackupState> {
    public static String TAG = ImapBackupTask.class.getSimpleName();

    Context context;
    private final ImapSmsService service;
    private final BackupItemsFetcher fetcher;
    private final MessageConverter converter;
    private final AuthPreferences authPreferences;
    private final Preferences preferences;
    private final ContactAccessor contactAccessor;
    private final TokenRefresher tokenRefresher;

    ImapBackupTask(@NotNull ImapSmsService service) {
        this.context = service.getApplicationContext();
        this.service = service;
        this.authPreferences = service.getAuthPreferences();
        this.preferences = service.getPreferences();

        this.fetcher = new BackupItemsFetcher(context,
                context.getContentResolver(),
                new BackupQueryBuilder(context));

        PersonLookup personLookup = new PersonLookup(service.getContentResolver());

        this.converter = new MessageConverter(context, preferences, authPreferences.getUserEmail(), personLookup, ContactAccessor.Get.instance());
        this.contactAccessor = ContactAccessor.Get.instance();
        this.tokenRefresher = new TokenRefresher(service, authPreferences);
    }

    ImapBackupTask(ImapSmsService service,
                   BackupItemsFetcher fetcher,
                   MessageConverter messageConverter,
                   AuthPreferences authPreferences,
                   Preferences preferences,
                   ContactAccessor accessor,
                   TokenRefresher refresher) {
        this.service = service;
        this.fetcher = fetcher;
        this.converter = messageConverter;
        this.authPreferences = authPreferences;
        this.preferences = preferences;
        this.contactAccessor = accessor;
        this.tokenRefresher = refresher;
    }

    @Override
    protected void onPreExecute() {
        App.bus.register(this);
    }

    @Subscribe
    public void userCanceled(UserCanceled canceled) {
        cancel(false);
    }

    @Override
    protected BackupState doInBackground(BackupConfig... params) {
        if (params == null || params.length == 0)
            throw new IllegalArgumentException("No config passed");
        final BackupConfig config = params[0];
        if (config.skip) {
            return skip(config.typesToBackup);
        } else {
            return acquireLocksAndBackup(config);
        }
    }

    private BackupState acquireLocksAndBackup(BackupConfig config) {
        try {
            service.acquireLocks(TAG);
            return fetchAndBackupItems(config);
        } finally {
            service.releaseLocks();
        }
    }

    private BackupState fetchAndBackupItems(BackupConfig config) {
        BackupCursors cursors = null;
        try {
            int itemsToSync = 0;

            int max = config.backupType.equals(BackupType.INCOMING) ? 5 : 1;
            for (int i = 0; i < max; i++) {
                cursors = new BulkFetcher(fetcher).fetch(config.typesToBackup, config.maxItemsPerSync);
                itemsToSync = cursors.count();
                if (itemsToSync > 0) {
                    break;
                } else {
                    cursors.close();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }

            itemsToSync += SMSApplication.from(context).notifications.getCount();

            if (itemsToSync > 0) {
                appLog(R.string.app_log_backup_messages, cursors.count(SMS), cursors.count(MMS), 0);
                if (config.debug) {
                    appLog(R.string.app_log_backup_messages_with_config, config);
                }

                return backupCursors(cursors, config.imapStore, config.backupType, itemsToSync);
            } else {
                appLog(R.string.app_log_skip_backup_no_items);

                if (preferences.isFirstBackup()) {
                    // If this is the first backup we need to write something to MAX_SYNCED_DATE
                    // such that we know that we've performed a backup before.
                    SMS.setMaxSyncedDate(service, Defaults.MAX_SYNCED_DATE);
                    MMS.setMaxSyncedDate(service, Defaults.MAX_SYNCED_DATE);
                    CALL.setMaxSyncedDate(service, Defaults.MAX_SYNCED_DATE);
                }
                Log.i(TAG, "Nothing to do.");
                return transition(FINISHED_BACKUP, null);
            }
        } catch (XOAuth2AuthenticationFailedException e) {
            return handleAuthError(config, e);
        } catch (AuthenticationFailedException e) {
            Log.w(TAG, e);
            return transition(ERROR, e);
        } catch (MessagingException e) {
            Log.w(TAG, e);
            return transition(ERROR, e);
        } finally {
            if (cursors != null)
                cursors.close();
        }
    }

    private BackupState handleAuthError(BackupConfig config, XOAuth2AuthenticationFailedException e) {
        if (e.getStatus() == 400) {
            Log.e(TAG, "need to perform xoauth2 token refresh", e);
            if (config.currentTry < 1) {
                try {
                    tokenRefresher.refreshOAuth2Token();
                    // we got a new token, let's handleAuthError one more time - we need to pass in a new store object
                    // since the auth params on it are immutable
                    appLogDebug("token refreshed, retrying");
                    return fetchAndBackupItems(config.retryWithStore(service.getBackupImapStore()));
                } catch (MessagingException ignored) {
                    Log.w(TAG, ignored);
                } catch (TokenRefreshException refreshException) {
                    appLogDebug("error refreshing token: " + refreshException + ", cause=" + refreshException.getCause());
                }
            } else {
                appLogDebug("no new token obtained, giving up");
            }
        } else {
            Log.e(TAG, "unexpected xoauth status code " + e.getStatus(), e);
        }
        return transition(ERROR, e);
    }

    private BackupState skip(Iterable<DataType> types) {
        appLog(R.string.app_log_skip_backup_skip_messages);
        for (DataType type : types) {
            type.setMaxSyncedDate(service, fetcher.getMostRecentTimestamp(type));
        }
        Log.i(TAG, "All messages skipped.");
        return new BackupState(FINISHED_BACKUP, 0, 0, BackupType.MANUAL, null, null);
    }

    private void appLog(int id, Object... args) {
        service.appLog(id, args);
    }

    private void appLogDebug(String message, Object... args) {
        service.appLogDebug(message, args);
    }

    private BackupState transition(SmsSyncState smsSyncState, Exception exception) {
        return service.transition(smsSyncState, exception);
    }

    @Override
    protected void onProgressUpdate(BackupState... progress) {
        if (progress != null && progress.length > 0 && !isCancelled()) {
            post(progress[0]);
        }
    }

    @Override
    protected void onPostExecute(BackupState result) {
        if (result != null) {
            post(result);
        }
        App.bus.unregister(this);
    }

    @Override
    protected void onCancelled() {
        post(transition(CANCELED_BACKUP, null));
        App.bus.unregister(this);
    }

    private void post(BackupState state) {
        if (state == null) return;
        App.bus.post(state);
    }

    private BackupState backupCursors(BackupCursors cursors, BackupImapStore store, BackupType backupType, int itemsToSync) throws MessagingException {
        Log.i(TAG, String.format(Locale.ENGLISH, "Starting backup (%d messages)", itemsToSync));
        publish(LOGIN);
        store.checkSettings();

        SIM sim = SMSApplication.from(context).getSIM();
        PersonLookup contacts = new PersonLookup(context.getContentResolver());

        try {
            publish(CALC);
            int backedUpItems = 0;
            while (!isCancelled() && cursors.hasNext()) {
                BackupCursors.CursorAndType cursor = cursors.next();
                if (LOCAL_LOGV) Log.v(TAG, "backing up: " + cursor);

                ConversionResult result = null;
                BackupImapStore.BackupFolder folder = null;
                switch (cursor.type) {
                    case SMS:
                        if (SMSPreferenceCompat.isEnabled(context)) {
                            final SmsStorage.Message msgMap = SmsStorage.getMessage(cursor.cursor);
                            Storage.SMSMessage sms = Storage.getMessage(sim, contacts, msgMap);
                            folder = store.getFolder(sms);
                            result = converter.convertMessages(msgMap, cursor.type);
                        } else {
                            result = new ConversionResult(cursor.type);
                        }
                        break;
                    case CALL:
                        if (CallLogsPreferenceCompat.isEnabled(context)) {
                            final CallsStorage.Call cc = CallsStorage.getCall(cursor.cursor);
                            Storage.CallEntry call = Storage.getMessage(sim, contacts, cc);
                            folder = store.getFolder(call);
                            result = converter.convertMessages(cc, cursor.type);
                        } else {
                            result = new ConversionResult(cursor.type);
                        }
                        break;
                }
                if (!result.isEmpty()) {
                    List<Message> messages = result.getMessages();

                    if (LOCAL_LOGV)
                        Log.v(TAG, String.format(Locale.ENGLISH, "sending %d %s message(s) to server.", messages.size(), cursor.type));

                    if (!cursors.filter())
                        folder.appendMessages(messages);

                    cursor.type.setMaxSyncedDate(service, result.getMaxDate());
                    backedUpItems += messages.size();
                } else {
                    Log.w(TAG, "no messages converted");
                    itemsToSync -= 1;
                }

                ImapSmsReplyService.checkFolders(context, store.getFolders());

                publishProgress(new BackupState(BACKUP, backedUpItems, itemsToSync, backupType, cursor.type, null));
            }

            if (NotificationsPreference.isEnabled(context)) {
                NotificationsStorage apps = SMSApplication.from(context).notifications; // backup notifications
                int count;
                while ((count = apps.getCount()) > 0) {
                    int last = count - 1;
                    NotificationsStorage.NotificationText o = apps.getMessage(last);
                    TagMimeMessage m = NotificationsStorage.formatMessage(context, o, authPreferences.getUserEmail());
                    long timestamp = o.timestamp;
                    Storage.SMSMessage sms = new Storage.SMSMessage(timestamp, "", "", "", "", -1);
                    store.getFolder(sms).appendMessages(Arrays.asList(new Message[]{m}));
                    backedUpItems++;
                    apps.delete(last);
                }
            }

            return new BackupState(FINISHED_BACKUP,
                    backedUpItems,
                    itemsToSync,
                    backupType, null, null);
        } finally {
            store.closeFolders();
        }
    }

    private void publish(SmsSyncState state) {
        publish(state, null);
    }

    private void publish(SmsSyncState state, Exception exception) {
        publishProgress(service.transition(state, exception));
    }
}
