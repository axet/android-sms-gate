package com.github.axet.smsgate.providers;

import android.content.Context;
import android.os.Build;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.SparseArray;

import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.mediatek.TelephonyManagerMT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SIM {
    public static final String TAG = SIM.class.getSimpleName();

    SparseArray<Integer> sims = new SparseArray<>(); // <slot,sub id>
    SparseArray<String> icc = new SparseArray<>(); // <slot,icc id>
    SparseArray<String> acc = new SparseArray<>(); // <slot,acc id>
    TelephonyManagerMT mt;
    SubscriptionManager ss;
    TelecomManager tm;
    public Context context;

    public SIM() {
    }

    public SIM(Context context) {
        this.context = context;
        if (Build.VERSION.SDK_INT >= 21)
            tm = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
        if (Build.VERSION.SDK_INT >= 23) {
            List<PhoneAccountHandle> list = tm.getCallCapablePhoneAccounts();
            for (PhoneAccountHandle h : list) {
                PhoneAccount a = tm.getPhoneAccount(h);
                acc.append(list.indexOf(h), a.getAccountHandle().getId());
            }
        }
        if (Build.VERSION.SDK_INT >= 22) {
            ss = SubscriptionManager.from(context); // READ_PHONE_STATE
            List<SubscriptionInfo> ll = ss.getActiveSubscriptionInfoList();
            if (ll == null)
                return;
            for (SubscriptionInfo s : ll)
                sims.append(s.getSimSlotIndex(), s.getSubscriptionId());
            for (SubscriptionInfo s : ll)
                icc.append(s.getSimSlotIndex(), s.getIccId());
            return;
        }
        try {
            mt = new TelephonyManagerMT(context);
            for (int i = 0; i < 5; i++) {
                if (mt.getSimState(i) != 0)
                    sims.append(i, i);
            }
        } catch (Throwable ignore) {
        }
    }

    public int getCount() {
        return sims.size();
    }

    public int getSimSlot(int i) {
        return sims.keyAt(i);
    }

    public int getSimID(int slot) { // get subscription id by slot
        Integer id = sims.get(slot);
        if (id == null)
            return -1;
        return id;
    }

    public int findID(int id) { // find slot by subscription id (sms has integer)
        for (int i = 0; i < sims.size(); i++) {
            int slot = sims.keyAt(i);
            if (sims.get(slot) == id)
                return slot;
        }
        return -1;
    }

    public int findID(String id) { // find slot by subscription id (phone calls has string)
        for (int i = 0; i < acc.size(); i++) {
            int slot = acc.keyAt(i);
            if (acc.get(slot).equals(id))
                return slot;
        }
        int v = Storage.toInt(id);
        for (int i = 0; i < sims.size(); i++) {
            int slot = sims.keyAt(i);
            if (sims.get(slot) == v)
                return slot;
        }
        for (int i = 0; i < icc.size(); i++) {
            int slot = icc.keyAt(i);
            if (icc.get(slot).equals(id))
                return slot;
        }
        return -1;
    }

    public String getSerial(int slot) {
        int id = sims.get(slot);
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getIccId();
        }
        return mt.getSimSerialNumber(id);
    }

    public String getOperatorName(int slot) {
        int id = sims.get(slot);
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getDisplayName().toString();
        }
        return mt.getSimOperatorName(id);
    }

    public String getOperatorCode(int slot) {
        int id = sims.get(slot);
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getCarrierName().toString();
        }
        return mt.getSimOperator(id);
    }
}
