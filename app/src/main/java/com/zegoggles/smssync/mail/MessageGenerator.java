package com.zegoggles.smssync.mail;

import static com.fsck.k9.mail.internet.MimeMessageHelper.setBody;
import static com.zegoggles.smssync.App.TAG;

import android.content.Context;
import android.os.Build;
import android.provider.CallLog;
import android.text.TextUtils;
import android.util.Log;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.TextBody;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.smsgate.app.CallsStorage;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.providers.SIM;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.preferences.AddressStyle;
import com.zegoggles.smssync.preferences.CallLogTypes;
import com.zegoggles.smssync.preferences.Preferences;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

class MessageGenerator {
    private final Context mContext;
    private final HeaderGenerator mHeaderGenerator;
    private final Address mUserAddress;
    private final PersonLookup mPersonLookup;
    private final boolean mPrefix;
    private final CallFormatter mCallFormatter;
    private final AddressStyle mAddressStyle;
    private final MmsSupport mMmsSupport;
    private final CallLogTypes mCallLogTypes;
    SIM sim;

    public MessageGenerator(Context context,
                            Address userAddress,
                            AddressStyle addressStyle,
                            HeaderGenerator headerGenerator,
                            PersonLookup personLookup,
                            boolean mailSubjectPrefix,
                            MmsSupport mmsSupport) {
        mHeaderGenerator = headerGenerator;
        mUserAddress = userAddress;
        mAddressStyle = addressStyle;
        mContext = context;
        mPersonLookup = personLookup;
        mPrefix = mailSubjectPrefix;
        mCallFormatter = new CallFormatter(mContext.getResources());
        mMmsSupport = mmsSupport;
        mCallLogTypes = CallLogTypes.getCallLogType(new Preferences(context));
        sim = SMSApplication.from(context).getSIM();
    }

    public
    @Nullable
    Message messageFromMapSms(SmsStorage.Message msgMap) throws MessagingException {
        final String address = msgMap.phone;
        if (TextUtils.isEmpty(address)) return null;

        PersonRecord record = mPersonLookup.lookupPerson(address);

        final Message msg = new MimeMessage();
        int slot = -1;
        if (sim.getCount() > 1) {
            slot = sim.findID(msgMap.simID);
            if (slot == -1)
                slot = msgMap.simID;
        }
        msg.setSubject(getSubject(DataType.SMS, slot, record));
        setBody(msg, new TextBody(msgMap.body));

        final int messageType = msgMap.type;

        // encode send / to address for easy reply function.
        // now to replay sms you just need reply email (it will be send to your inbox)
        Address addr = new Address(mUserAddress);
        addr.setPersonal(record.getNumber());

        if (SmsStorage.IN == messageType) {
            // Received message
            msg.setFrom(addr);
            msg.setRecipient(Message.RecipientType.TO, mUserAddress);
        } else {
            // Sent message
            msg.setRecipient(Message.RecipientType.TO, addr);
            msg.setFrom(mUserAddress);
        }

        Date sentDate;
        try {
            sentDate = new Date(msgMap.date);
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = new Date();
        }
        mHeaderGenerator.setHeaders(msg, DataType.SMS, address, record, sentDate, messageType);
        mHeaderGenerator.setSmsHeaders(msg, msgMap);
        msg.setHeader(Headers.SIM_ID, String.valueOf(msgMap.simID));
        //msg.setUsing7bitTransport();
        return msg;
    }

    public
    @Nullable
    Message messageFromMapCall(CallsStorage.Call msgMap) throws MessagingException {
        final String address = msgMap.phone;
        if (TextUtils.isEmpty(address)) return null;

        PersonRecord record = mPersonLookup.lookupPerson(address);

        final Message msg = new MimeMessage();
        int slot = -1;
        if (sim.getCount() > 1) {
            slot = sim.findID(msgMap.simID);
            if (slot == -1)
                slot = Storage.toInt(msgMap.simID);
        }
        msg.setSubject(getSubject(DataType.CALL, slot, record));
        setBody(msg, new TextBody(Storage.formatBody(sim, Build.DEVICE, Storage.getMessage(sim, mPersonLookup, msgMap))));

        final int messageType = msgMap.type;

        // encode send / to address for easy reply function.
        // now to replay sms you just need reply email (it will be send to your inbox)
        Address addr = new Address(mUserAddress);
        addr.setPersonal(record.getNumber());

        if (CallsStorage.IN == messageType) {
            // Received message
            msg.setFrom(addr);
            msg.setRecipient(Message.RecipientType.TO, mUserAddress);
        } else {
            // Sent message
            msg.setRecipient(Message.RecipientType.TO, addr);
            msg.setFrom(mUserAddress);
        }

        Date sentDate;
        try {
            sentDate = new Date(msgMap.date);
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = new Date();
        }
        mHeaderGenerator.setHeaders(msg, DataType.CALL, address, record, sentDate, messageType);
        mHeaderGenerator.setCallLogHeaders(msg, msgMap);
        msg.setHeader(Headers.SIM_ID, msgMap.simID);
        //msg.setUsing7bitTransport();
        return msg;
    }

    private String getSubject(@NotNull DataType type, int slot, @NotNull PersonRecord record) {
        String suffix = "";
        if (slot >= 0)
            suffix = String.valueOf(slot);
        return mContext.getString(type.withField, record.getName(), suffix);
    }
}
