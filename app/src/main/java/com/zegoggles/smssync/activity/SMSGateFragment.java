/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zegoggles.smssync.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.activities.MainActivity;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.dialogs.FirebaseConnectDialog;
import com.github.axet.smsgate.services.FileCallsService;
import com.github.axet.smsgate.services.FileReplyService;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.OnBootReceiver;
import com.github.axet.smsgate.services.ImapSmsReplyService;
import com.github.axet.smsgate.services.FileSmsService;
import com.github.axet.smsgate.services.USSDService;
import com.github.axet.smsgate.widgets.CallLogsPreferenceCompat;
import com.github.axet.smsgate.widgets.IMAPSyncPreferenceCompat;
import com.github.axet.smsgate.widgets.NotificationsPreference;
import com.github.axet.smsgate.widgets.SMSPreferenceCompat;
import com.github.axet.smsgate.widgets.StoragePathPreferenceCompat;
import com.github.axet.smsgate.widgets.USSDPreferenceCompat;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.activity.auth.AccountManagerAuthActivity;
import com.zegoggles.smssync.activity.auth.WebAuthActivity;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.BackupManagerWrapper;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.ImapSmsService;
import com.zegoggles.smssync.tasks.OAuthCallbackTask;
import com.zegoggles.smssync.tasks.RequestTokenTask;
import com.zegoggles.smssync.utils.AppLog;

import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.mail.DataType.MMS;
import static com.zegoggles.smssync.preferences.Preferences.Keys.CONNECTED;

/**
 * This is the main activity showing the status of the SMS Sync service and
 * providing controls to configure it.
 */
public class SMSGateFragment extends PreferenceFragmentCompat implements MainActivity.OnBackHandler, SharedPreferences.OnSharedPreferenceChangeListener {
    public static final int MIN_VERSION_MMS = Build.VERSION_CODES.ECLAIR;
    public static final int MIN_VERSION_BACKUP = Build.VERSION_CODES.FROYO;

    private static final int REQUEST_CHANGE_DEFAULT_SMS_PACKAGE = 1;
    private static final int REQUEST_PICK_ACCOUNT = 2;
    private static final int REQUEST_WEB_AUTH = 3;
    public static final int RESULT_PERMS_STOR = 4;
    public static final int RESULT_PERMS_FIRE = 5;
    public static final int RESULT_PERMS_CONN = 6;
    public static final int RESULT_PERMS_STOR_BASIC = 7; // contacts, read sms, etc
    public static final int RESULT_PERMS_IMAP = 8;
    public static final int RESULT_CALLS = 9;
    public static final int RESULT_SMS = 10;

    public static final String[] PERMISSIONS_STORAGE =  Storage.PERMISSIONS_RW;

    enum Actions {
        Backup,
    }

    private Actions mActions;

    List<PreferenceScreen> back = new ArrayList<>();

    private AuthPreferences authPreferences;
    private Preferences preferences;
    private StatusPreference statusPref;
    Handler handler = new Handler();
    private
    @Nullable
    Uri mAuthorizeUri;

    FirebaseConnectDialog firebase;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a == null)
                return;
            if (a.equals(FirebaseService.FIREBASE)) {
                updateFirebase();
            }
        }
    };

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        final Context context = getActivity();

        authPreferences = new AuthPreferences(context);
        preferences = new Preferences(context);
        addPreferencesFromResource(R.xml.preferences);

        statusPref = new StatusPreference(this);
        statusPref.setOrder(-1);
        getPreferenceScreen().addPreference(statusPref);

        int version = Build.VERSION.SDK_INT;
        if (version < MIN_VERSION_MMS) {
            CheckBoxPreference backupMms = (CheckBoxPreference) findPreference(MMS.backupEnabledPreference);
            backupMms.setEnabled(false);
            backupMms.setChecked(false);
            backupMms.setSummary(R.string.ui_backup_mms_not_supported);
        }
        if (preferences.shouldShowUpgradeMessage()) show(Dialogs.UPGRADE_FROM_SMSBACKUP);
        setPreferenceListeners(version >= MIN_VERSION_BACKUP);

        checkAndDisplayDroidWarning();

        preferences.migrateMarkAsRead();

//        if (preferences.shouldShowAboutDialog()) {
//            show(Dialogs.ABOUT);
//        }

        checkDefaultSmsApp();

        setupStrictMode();
        App.bus.register(this);

        IntentFilter ff = new IntentFilter();
        ff.addAction(FirebaseService.FIREBASE);
        context.registerReceiver(broadcastReceiver, ff);

        OnBootReceiver.fragment(getActivity());

        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(context);
        prefs.registerOnSharedPreferenceChangeListener(this);

        final StoragePathPreferenceCompat storage = (StoragePathPreferenceCompat) findPreference(SMSApplication.PREF_STORAGE);
        storage.setStorage(new Storage(getContext()));
        storage.setPermissionsDialog(this, PERMISSIONS_STORAGE, RESULT_PERMS_STOR);
        if (Build.VERSION.SDK_INT >= 21)
            storage.setStorageAccessFramework(this, RESULT_PERMS_STOR);
        storage.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (Storage.permitted(SMSGateFragment.this, PERMISSIONS_STORAGE, RESULT_PERMS_STOR_BASIC))
                    storage.onClickDialog();
                return false;
            }
        });
        storage.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                OnBootReceiver.storage(getActivity(), newValue == null || ((String) newValue).isEmpty());
                return true;
            }
        });

        final IMAPSyncPreferenceCompat imap = (IMAPSyncPreferenceCompat) findPreference(SMSApplication.PREF_IMAP);
        imap.onResume();
        imap.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (Storage.permitted(SMSGateFragment.this, IMAPSyncPreferenceCompat.PERMISSIONS, RESULT_PERMS_IMAP))
                    imap.onClickDialog();
                return false;
            }
        });

        CallLogsPreferenceCompat call = (CallLogsPreferenceCompat) findPreference(SMSApplication.PREF_CALLLOGS);
        call.f = this;
        call.onResume();
        SMSPreferenceCompat sms = (SMSPreferenceCompat) findPreference(SMSApplication.PREF_SMSSYNC);
        sms.f = this;
        sms.onResume();

        NotificationsPreference apps = (NotificationsPreference) findPreference(SMSApplication.PREF_APPS);
        apps.onResume();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        App.bus.register(statusPref);

        CallLogsPreferenceCompat call = (CallLogsPreferenceCompat) findPreference(SMSApplication.PREF_CALLLOGS);
        call.onResume();
        SMSPreferenceCompat sms = (SMSPreferenceCompat) findPreference(SMSApplication.PREF_SMSSYNC);
        sms.onResume();
        NotificationsPreference apps = (NotificationsPreference) findPreference(SMSApplication.PREF_APPS);
        apps.onResume();
        USSDPreferenceCompat ussd = (USSDPreferenceCompat) findPreference("ussd");
        ussd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        App.bus.unregister(statusPref);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (broadcastReceiver != null) {
            getActivity().unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }

        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.unregisterOnSharedPreferenceChangeListener(this);

        try {
            App.bus.unregister(this);
        } catch (Exception ignored) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                show(Dialogs.ABOUT);
                return true;
            case R.id.menu_reset:
                show(Dialogs.RESET);
                return true;
            case R.id.menu_view_log:
                show(Dialogs.VIEW_LOG);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // http://stackoverflow.com/questions/3354955/onactivityresult-called-prematurely
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data + ")");
        if (resultCode == Activity.RESULT_CANCELED) return;

        switch (requestCode) {
            case REQUEST_CHANGE_DEFAULT_SMS_PACKAGE: {
                preferences.setSeenSmsDefaultPackageChangeDialog();
                break;
            }
            case REQUEST_WEB_AUTH: {
                Uri uri = data.getData();
                if (uri != null && uri.toString().startsWith(Consts.CALLBACK_URL)) {
                    show(Dialogs.ACCESS_TOKEN);
                    new OAuthCallbackTask(getActivity()).execute(data);
                }
                break;
            }
            case REQUEST_PICK_ACCOUNT: {
                if (AccountManagerAuthActivity.ACTION_ADD_ACCOUNT.equals(data.getAction()))
                    handleAccountManagerAuth(data);
                else if (AccountManagerAuthActivity.ACTION_FALLBACKAUTH.equals(data.getAction()))
                    handleFallbackAuth();
                break;
            }
            case RESULT_PERMS_STOR: {
                StoragePathPreferenceCompat storage = (StoragePathPreferenceCompat) findPreference(SMSApplication.PREF_STORAGE);
                storage.onActivityResult(resultCode, data);
                break;
            }
        }
    }

    @Subscribe
    public void onAuthorizedURLReceived(RequestTokenTask.AuthorizedURLReceived authorizedURLReceived) {
        dismiss(Dialogs.REQUEST_TOKEN);
        this.mAuthorizeUri = authorizedURLReceived.uri;
        if (mAuthorizeUri != null) {
            show(Dialogs.CONNECT);
        } else {
            show(Dialogs.CONNECT_TOKEN_ERROR);
        }
    }

    @Subscribe
    public void onOAuthCallback(OAuthCallbackTask.OAuthCallbackEvent event) {
        dismiss(Dialogs.ACCESS_TOKEN);
        if (event.valid()) {
            authPreferences.setOauthUsername(event.username);
            authPreferences.setOauthTokens(event.token, event.tokenSecret);
            onAuthenticated();
        } else {
            show(Dialogs.ACCESS_TOKEN_ERROR);
        }
    }

    private void onAuthenticated() {
        updateConnected();
        // Invite use to perform a backup, but only once
        if (preferences.isFirstBackup()) {
            show(Dialogs.FIRST_SYNC);
        } else {
            OnBootReceiver.imap(getActivity(), true);
            OnBootReceiver.storage(getActivity(), true);
        }
    }

    void disconnect() {
        authPreferences.clearOauthData();
        DataType.clearLastSyncData(getActivity());
        updateConnected();
        stop();
    }

    void stop() {
        OnBootReceiver.imap(getActivity(), false);
        OnBootReceiver.storage(getActivity(), false);
    }

    String getLastSyncText(final long lastSync) {
        return getString(R.string.status_idle_details,
                lastSync < 0 ? getString(R.string.status_idle_details_never) :
                        DateFormat.getDateTimeInstance().format(new Date(lastSync)));
    }

    private ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    protected String getEnabledBackupSummary() {
        final List<String> enabled = new ArrayList<String>();
        for (DataType dataType : DataType.enabled(getActivity())) {
            enabled.add(getString(dataType.resId));
        }
        StringBuilder summary = new StringBuilder();
        if (!enabled.isEmpty()) {
            summary.append(getString(R.string.ui_enable_auto_sync_summary, TextUtils.join(", ", enabled)));
            if (!getConnectivityManager().getBackgroundDataSetting()) {
                summary.append(' ').append(getString(R.string.ui_enable_auto_sync_bg_data));
            }
            if (preferences.isInstalledOnSDCard()) {
                summary.append(' ').append(getString(R.string.sd_card_disclaimer));
            }
        } else {
            summary.append(getString(R.string.ui_enable_auto_sync_no_enabled_summary));
        }
        return summary.toString();
    }

    private void addSummaryListener(final Runnable r, String... prefs) {
        for (String p : prefs) {
            findPreference(p).setOnPreferenceChangeListener(
                    new Preference.OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, final Object newValue) {
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    r.run();
                                    // TODO onContentChanged();
                                }
                            });
                            return true;
                        }
                    });
        }
    }

    private void initiateBackup() {
        if (!authPreferences.isLoginInformationSet()) {
            if (Storage.isEnabled(getContext())) {
                FileSmsService.incoming(getContext(), 0);
                FileCallsService.incoming(getContext(), 0);
            } else {
                show(Dialogs.MISSING_CREDENTIALS);
            }
        } else {
            if (preferences.isFirstBackup())
                show(Dialogs.FIRST_SYNC);
            else
                startBackup(false);
            FileSmsService.incoming(getContext(), 0);
        }
    }

    void performAction(Actions act) {
        performAction(act, preferences.confirmAction());
    }

    private void performAction(Actions act, boolean needConfirm) {
        if (needConfirm) {
            this.mActions = act;
            show(Dialogs.CONFIRM_ACTION);
        } else {
            if (Actions.Backup.equals(act))
                initiateBackup();
        }
    }

    private void startBackup(boolean skip) {
        Context context = getActivity();
        ImapSmsService.startManual(context, skip);
        ImapSmsReplyService.start(context);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private boolean isSmsBackupDefaultSmsApp() {
        Context context = getActivity();
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT &&
                context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (Dialogs.values()[id]) {
            case VIEW_LOG:
                View view = dialog.findViewById(AppLog.ID);
                if (view instanceof TextView) {
                    AppLog.readLog(App.LOG, (TextView) view);
                }
        }
    }

    protected Dialog onCreateDialog(final int id) {
        final Context context = getActivity();

        String title, msg;
        switch (Dialogs.values()[id]) {
            case MISSING_CREDENTIALS:
                title = getString(R.string.ui_dialog_missing_credentials_title);
                msg = authPreferences.useXOAuth() ?
                        getString(R.string.ui_dialog_missing_credentials_msg_xoauth) :
                        getString(R.string.ui_dialog_missing_credentials_msg_plain);
                break;
            case INVALID_IMAP_FOLDER:
                title = getString(R.string.ui_dialog_invalid_imap_folder_title);
                msg = getString(R.string.ui_dialog_invalid_imap_folder_msg);
                break;
            case FIRST_SYNC:
                DialogInterface.OnClickListener firstSyncListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startBackup(which == DialogInterface.BUTTON2);
                            }
                        };
                final int maxItems = preferences.getMaxItemsPerSync();
                final String syncMsg = maxItems < 0 ?
                        getString(R.string.ui_dialog_first_sync_msg) :
                        getString(R.string.ui_dialog_first_sync_msg_batched, maxItems);

                return new AlertDialog.Builder(context)
                        .setTitle(R.string.ui_dialog_first_sync_title)
                        .setMessage(syncMsg)
                        .setCancelable(false)
                        .setPositiveButton(R.string.ui_sync, firstSyncListener)
                        .setNegativeButton(R.string.ui_skip, firstSyncListener)
                        .create();
            case ABOUT:
                return AboutPreferenceCompat.buildDialog(context, R.raw.about).create();

            case VIEW_LOG:
                return AppLog.displayAsDialog(App.LOG, context);

            case RESET:
                return new AlertDialog.Builder(context)
                        .setTitle(R.string.ui_dialog_reset_title)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                reset();
                                dismissDialog(id);
                            }
                        })
                        .setMessage(R.string.ui_dialog_reset_message)
                        .setNegativeButton(android.R.string.cancel, null)
                        .create();

            case REQUEST_TOKEN:
                ProgressDialog req = new ProgressDialog(context);
                req.setTitle(null);
                req.setMessage(getString(R.string.ui_dialog_request_token_msg));
                req.setIndeterminate(true);
                req.setCancelable(false);
                return req;
            case ACCESS_TOKEN:
                ProgressDialog acc = new ProgressDialog(context);
                acc.setTitle(null);
                acc.setMessage(getString(R.string.ui_dialog_access_token_msg));
                acc.setIndeterminate(true);
                acc.setCancelable(false);
                return acc;
            case ACCESS_TOKEN_ERROR:
                title = getString(R.string.ui_dialog_access_token_error_title);
                msg = getString(R.string.ui_dialog_access_token_error_msg);
                break;
            case CONNECT:
                return new AlertDialog.Builder(context)
                        .setCustomTitle(null)
                        .setMessage(getString(R.string.ui_dialog_connect_msg, getString(R.string.app_name)))
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (mAuthorizeUri != null) {
                                    startActivityForResult(new Intent(context, WebAuthActivity.class)
                                            .setData(mAuthorizeUri), REQUEST_WEB_AUTH);
                                }
                                dismissDialog(id);
                            }
                        }).create();
            case CONNECT_TOKEN_ERROR:
                return new AlertDialog.Builder(context)
                        .setCustomTitle(null)
                        .setMessage(R.string.ui_dialog_connect_token_error)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).create();

            case ACCOUNTMANAGER_TOKEN_ERROR:
                return new AlertDialog.Builder(context)
                        .setCustomTitle(null)
                        .setMessage(R.string.ui_dialog_account_manager_token_error)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                handleFallbackAuth();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .create();
            case DISCONNECT:
                return new AlertDialog.Builder(context)
                        .setCustomTitle(null)
                        .setMessage(R.string.ui_dialog_disconnect_msg)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                disconnect();
                            }
                        }).create();
            case UPGRADE_FROM_SMSBACKUP:
                title = getString(R.string.ui_dialog_upgrade_title);
                msg = getString(R.string.ui_dialog_upgrade_msg);
                break;
            case BROKEN_DROIDX:
                title = getString(R.string.ui_dialog_brokendroidx_title);
                msg = getString(R.string.ui_dialog_brokendroidx_msg);
                break;
            case CONFIRM_ACTION:
                return new AlertDialog.Builder(context)
                        .setTitle(R.string.ui_dialog_confirm_action_title)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (SMSGateFragment.this.mActions != null) {
                                    performAction(SMSGateFragment.this.mActions, false);
                                }
                            }
                        })
                        .setMessage(R.string.ui_dialog_confirm_action_msg)
                        .setNegativeButton(android.R.string.cancel, null)
                        .create();
            case SMS_DEFAULT_PACKAGE_CHANGE:
                return new AlertDialog.Builder(context)
                        .setTitle(R.string.ui_dialog_sms_default_package_change_title)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestDefaultSmsPackageChange();
                            }
                        })
                        .setMessage(R.string.ui_dialog_sms_default_package_change_msg)
                        .create();
            default:
                return null;
        }
        return createMessageDialog(id, title, msg);
    }

    private void reset() {
        DataType.clearLastSyncData(getActivity());
        preferences.reset();
    }

    private Dialog createMessageDialog(final int id, String title, String msg) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();
    }

    private void updateMaxItems(String prefKey, int currentValue, String newValue) {
        Preference pref = findPreference(prefKey);
        if (newValue == null) {
            newValue = String.valueOf(currentValue);
        }
        // XXX
        pref.setTitle("-1".equals(newValue) ? getString(R.string.all_messages) : newValue);
    }

    private CheckBoxPreference updateConnected() {
        CheckBoxPreference connected = (CheckBoxPreference) findPreference(CONNECTED.key);

        connected.setVisible(authPreferences.useXOAuth());
        connected.setChecked(authPreferences.hasOauthTokens() || authPreferences.hasOAuth2Tokens());

        final String username = authPreferences.getUsername();
        String summary = connected.isChecked() && !TextUtils.isEmpty(username) ?
                getString(R.string.gmail_already_connected, username) :
                getString(R.string.gmail_needs_connecting);
        connected.setSummary(summary);

        return connected;
    }

    public CheckBoxPreference updateFirebase() {
        if (firebase != null)
            firebase.update();

        CheckBoxPreference connected = (CheckBoxPreference) findPreference(SMSApplication.PREF_FIREBASE);

        if (!SMSApplication.firebaseEnabled(getContext()))
            connected.setVisible(false);

        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(getActivity());
        connected.setChecked(prefs.getBoolean(SMSApplication.PREF_FIREBASE, false));

        String summary;

        if (connected.isChecked()) {
            if (FirebaseService.connected()) {
                summary = "Web connected";
            } else {
                if (FirebaseService.isPause(getActivity()))
                    summary = "Web paused (Wifi Only)";
                else
                    summary = "Web connecting...";
            }
        } else {
            summary = "Web disconnected";
        }

        connected.setSummary(summary);

        return connected;
    }

    private void show(Dialogs d) {
        showDialog(d.ordinal());
    }

    public SharedPreferences getPreferences(int i) {
        return PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    public void runOnUiThread(Runnable run) {
        handler.post(run);
    }

    Map<Integer, Dialog> dialogs = new HashMap<>();

    public void showDialog(int id) {
        Dialog d = onCreateDialog(id);
        onPrepareDialog(id, d);
        d.show();
        dialogs.put(id, d);
    }

    public void dismissDialog(int id) {
        Dialog d = dialogs.get(id);
        d.dismiss();
        dialogs.remove(id);
    }

    private void dismiss(Dialogs d) {
        try {
            dismissDialog(d.ordinal());
        } catch (IllegalArgumentException e) {
            // ignore
        }
    }

    private void setPreferenceListeners(boolean backup) {
        if (backup) {
            final Context context = getActivity();
            PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(
                    new SharedPreferences.OnSharedPreferenceChangeListener() {
                        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                            BackupManagerWrapper.dataChanged(context);
                        }
                    }
            );
        }

        updateConnected().setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object change) {
                boolean newValue = (Boolean) change;
                if (newValue) {
                    if (Storage.permitted(SMSGateFragment.this, SMSApplication.permissions(getContext()), RESULT_PERMS_CONN))
                        connect();
                } else {
                    show(Dialogs.DISCONNECT);
                }
                return false;
            }
        });

        updateFirebase().setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (Storage.permitted(SMSGateFragment.this, SMSApplication.permissions(getContext()), RESULT_PERMS_FIRE))
                    firebaseShow();
                return false;
            }
        });
    }

    void connect() {
        if (Build.VERSION.SDK_INT >= 5) {
            // use account manager on newer phones
            startActivityForResult(new Intent(getActivity(), AccountManagerAuthActivity.class), REQUEST_PICK_ACCOUNT);
        } else {
            // fall back to webview on older ones
            handleFallbackAuth();
        }
    }

    @TargetApi(11)
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    private void setupStrictMode() {
    }

    private void checkAndDisplayDroidWarning() {
        if ("DROIDX".equals(Build.MODEL) ||
                "DROID2".equals(Build.MODEL) &&
                        Build.VERSION.SDK_INT == Build.VERSION_CODES.FROYO &&
                        !getPreferences(MODE_PRIVATE).getBoolean("droidx_warning_displayed", false)) {
            getPreferences(MODE_PRIVATE).edit().putBoolean("droidx_warning_displayed", true).commit();
            show(Dialogs.BROKEN_DROIDX);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void requestDefaultSmsPackageChange() {
        final Intent changeIntent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT)
                .putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, getActivity().getPackageName());

        startActivityForResult(changeIntent, REQUEST_CHANGE_DEFAULT_SMS_PACKAGE);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void restoreDefaultSmsProvider(String smsPackage) {
        Log.d(TAG, "restoring SMS provider " + smsPackage);
        if (!TextUtils.isEmpty(smsPackage)) {
            final Intent changeDefaultIntent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT)
                    .putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, smsPackage);
            startActivity(changeDefaultIntent);
        }
    }

    private void handleAccountManagerAuth(Intent data) {
        String token = data.getStringExtra(AccountManagerAuthActivity.EXTRA_TOKEN);
        String account = data.getStringExtra(AccountManagerAuthActivity.EXTRA_ACCOUNT);
        if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(account)) {
            authPreferences.setOauth2Token(account, token);
            onAuthenticated();
        } else {
            String error = data.getStringExtra(AccountManagerAuthActivity.EXTRA_ERROR);
            if (!TextUtils.isEmpty(error)) {
                show(Dialogs.ACCOUNTMANAGER_TOKEN_ERROR);
            }
        }
    }

    private void handleFallbackAuth() {
        show(Dialogs.REQUEST_TOKEN);
        new RequestTokenTask(getActivity()).execute(Consts.CALLBACK_URL);
    }

    private void checkDefaultSmsApp() {
        if (isSmsBackupDefaultSmsApp()) {
            restoreDefaultSmsProvider(preferences.getSmsDefaultPackage());
        }
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference instanceof PreferenceScreen) {
            back.add(getPreferenceScreen());
            setPreferenceScreen((PreferenceScreen) preference);
            ((MainActivity) getActivity()).onBackHandler(this);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onBackPressed() {
        PreferenceScreen b = back.get(back.size() - 1);
        setPreferenceScreen(b);
        back.remove(b);
        if (back.size() == 0)
            ((MainActivity) getActivity()).onBackHandler(null);
        else
            ((MainActivity) getActivity()).onBackHandler(this);
    }

    void firebaseShow() {
        firebase = new FirebaseConnectDialog(getActivity());
        firebase.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Storage.permitted(getActivity(), permissions)) {
            switch (requestCode) {
                case RESULT_PERMS_FIRE:
                    firebaseShow();
                    break;
                case RESULT_PERMS_CONN:
                    connect();
                    break;
                case RESULT_PERMS_STOR:
                    StoragePathPreferenceCompat storage = (StoragePathPreferenceCompat) findPreference(SMSApplication.PREF_STORAGE);
                    storage.onRequestPermissionsResult(permissions, grantResults);
                    break;
                case RESULT_PERMS_STOR_BASIC:
                    StoragePathPreferenceCompat storage2 = (StoragePathPreferenceCompat) findPreference(SMSApplication.PREF_STORAGE);
                    storage2.onClickDialog();
                    break;
                case RESULT_PERMS_IMAP:
                    IMAPSyncPreferenceCompat imap = (IMAPSyncPreferenceCompat) findPreference(SMSApplication.PREF_IMAP);
                    imap.onClickDialog();
                    break;
                case RESULT_CALLS:
                    CallLogsPreferenceCompat call = (CallLogsPreferenceCompat) findPreference(SMSApplication.PREF_CALLLOGS);
                    call.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    break;
                case RESULT_SMS:
                    SMSPreferenceCompat sms = (SMSPreferenceCompat) findPreference(SMSApplication.PREF_SMSSYNC);
                    sms.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    break;
            }
        } else {
            Toast.makeText(getActivity(), R.string.not_permitted, Toast.LENGTH_SHORT).show();
        }
    }

    // initial startup event, can be first after app call. check and update permissions.
    public static void checkPermissions(Context context) {
        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(context);
        if (!Storage.permitted(context, SMSApplication.permissions(context))) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(SMSApplication.PREF_FIREBASE, false);
            edit.putBoolean(CONNECTED.key, false);
            edit.commit();
            AuthPreferences authPreferences = new AuthPreferences(context);
            authPreferences.clearOauthData();
        }
    }

    @Override
    public Preference findPreference(CharSequence key) {
        if (back.isEmpty()) {
            return super.findPreference(key);
        } else {
            return back.get(0).findPreference(key);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SMSApplication.PREF_WIFI))
            updateFirebase();
        if (key.equals(AuthPreferences.SERVER_AUTHENTICATION))
            updateConnected();
        if (key.equals(SMSApplication.PREF_IMAP) || key.equals(AuthPreferences.SERVER_AUTHENTICATION)) {
            final IMAPSyncPreferenceCompat imap = (IMAPSyncPreferenceCompat) findPreference(SMSApplication.PREF_IMAP);
            imap.onResume();
        }
    }
}
