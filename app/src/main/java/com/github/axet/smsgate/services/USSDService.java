package com.github.axet.smsgate.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.AlertDialog;
import android.app.Notification;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.TextView;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.widgets.NotificationsPreference;

import java.util.Collections;
import java.util.List;

public class USSDService extends AccessibilityService {
    public static String TAG = USSDService.class.getSimpleName();

    public static String ADD = "ADD";
    public static String REMOVE = "REMOVE";
    public static String UPDATE = "UPDATE";

    public static void start(Context context) {
        Intent intent = new Intent(context, USSDService.class);
        context.startService(intent);
    }

    public static boolean enabled(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        String packageName = context.getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    public static void show(Context context) {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        context.startActivity(intent);
    }

    public static void startIfEnabled(Context context) {
        if (enabled(context))
            start(context);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            String cs = event.getClassName().toString();
            if (cs.equals(AlertDialog.class.getCanonicalName()))
                processUSSD(event, event.getText());
        } else if (Build.VERSION.SDK_INT >= 19) {
            AccessibilityNodeInfo source = event.getSource();
            if (source != null && source.getClassName().equals(TextView.class.getCanonicalName()))
                processUSSD(event, Collections.singletonList(source.getText()));
        }
    }

    public void processUSSD(AccessibilityEvent event, List<CharSequence> str) {
        String id = SMSApplication.toHexString(event.getPackageName().toString().hashCode());
        Notification n = new Notification();
        if (Build.VERSION.SDK_INT >= 19) {
            n.extras.putString("android.title", event.getClassName().toString()); // TODO show source / operator / dialog title
            n.extras.putString("android.bigText", str.toString());
        } else {
            n.tickerText = str.toString();
        }
        SMSApplication.from(this).notifications.notification(NotificationService.UPDATE, FirebaseService.toString(event.getPackageName()), id, n);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
        info.notificationTimeout = 100;
        info.packageNames = new String[]{NotificationsPreference.PHONE};
        setServiceInfo(info);
    }
}