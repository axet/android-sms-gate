package com.github.axet.smsgate.app;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.Firebase;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.app.Prefs;
import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.services.WifiKeepService;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.smsgate.BuildConfig;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.activities.MainActivity;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.providers.SMS;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.ScheduleService;
import com.github.axet.smsgate.widgets.CallLogsPreferenceCompat;
import com.github.axet.smsgate.widgets.DefaultSMSPreferenceCompat;
import com.github.axet.smsgate.widgets.IMAPSyncPreferenceCompat;
import com.github.axet.smsgate.widgets.SMSPreferenceCompat;
import com.zegoggles.smssync.App;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.TreeSet;

// adb shell "am broadcast -a com.github.axet.smsgate.key -e key '3333441...2341234'"
// adb shell "am broadcast -a com.github.axet.smsgate.sms -e phone +199988877766 -e msg 'hello'"
public class SMSApplication extends App {
    public static final String SCHEDULER_COUNT = "SCHEDULER_COUNT";
    public static final String SCHEDULER_ITEM = "SCHEDULER_";
    public static final String SEC = "SEC";
    public static final String SMS_LAST = "SMS_LAST";
    public static final String STORAGE_SMS_LAST = "STORAGE_SMS_LAST";
    public static final String STORAGE_CALLS_LAST = "STORAGE_CALLS_LAST";
    public static final String STORAGE_SMS_LAST_REPLY = "STORAGE_SMS_LAST_REPLY";

    public static final String PREF_FIREBASE = "firebase";
    public static final String PREF_NOTIFICATION_LISTENER = "notification_listener";
    public static final String PREF_WIFIRESTART = "wifi_restart";
    public static final String PREF_WIFI = "wifi_only";
    public static final String PREF_ADMIN = "admin";
    public static final String PREF_OPTIMIZATION = "optimization";
    public static final String PREF_NEXT = "next";
    public static final String PREF_DEFAULTSMS = "defaultsms";
    public static final String PREF_FIREBASESETTINGS = "firebase_settings";
    public static final String PREF_REBOOT = "reboot";
    public static final String PREF_STORAGE = "storage";
    public static final String PREF_IMAPFORMAT = "imap_folder";
    public static final String PREF_IMAPFORMAT_DEFAULT = "Inbox";
    public static final String PREF_NAMEFORMAT = "name";
    public static final String PREF_NAMEFORMAT_DEFAULT = "%d %t (%m) %p";
    public static final String PREF_DELETE = "delete";
    public static final String PREF_DELETE_OFF = Prefs.PrefString(R.string.delete_off);
    public static final String PREF_DELETE_1DAY = Prefs.PrefString(R.string.delete_1day);
    public static final String PREF_DELETE_1WEEK = Prefs.PrefString(R.string.delete_1week);
    public static final String PREF_DELETE_1MONTH = Prefs.PrefString(R.string.delete_1month);
    public static final String PREF_DELETE_3MONTH = Prefs.PrefString(R.string.delete_3month);
    public static final String PREF_DELETE_6MONTH = Prefs.PrefString(R.string.delete_6month);
    public static final String PREF_REPLY = "reply"; // reply last query time
    public static final String PREF_IMAP = "imapsync";
    public static final String PREF_CALLLOGS = "call_logs"; // enable disable call logs sync
    public static final String PREF_SMSSYNC = "sms_sync"; // enable disable sms sync

    public static final String PREF_APPS = "applications"; // enabled
    public static final String APPS_INDEX = "APPS_";
    public static final String APPS_COUNT = "APPS_COUNT";

    public static final String PREF_CONTACTS_ONLY = "contacts_only";

    public static final String ACTION_KEY = BuildConfig.APPLICATION_ID + ".key";
    public static final String ACTION_SENDSMS = BuildConfig.APPLICATION_ID + ".sms";
    public static final String[] COMMANDS = new String[]{ACTION_KEY, ACTION_SENDSMS};

    public static final String[] SEND_PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS};

    Thread t;
    SIM sim;

    public BitcoinStorage keys = null;
    public ScheduleSMSStorage schedules;
    public NotificationsStorage notifications;
    public NotificationChannelCompat persistent;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a != null) {
                if (a.equals(ACTION_KEY)) {
                    keys.load(intent.getStringExtra("key"));
                    keys.save();
                    FirebaseService.start(context);
                }
                if (a.equals(ACTION_SENDSMS)) {
                    if (Storage.permitted(context, SEND_PERMISSIONS))
                        SMS.send(context, intent.getStringExtra("phone"), intent.getStringExtra("msg"));
                    else
                        MainActivity.start(context, SEND_PERMISSIONS, intent);
                }
            }
        }
    };

    public static SMSApplication from(Context context) {
        return (SMSApplication) App.from(context);
    }

    public static boolean firebaseEnabled(Context context) {
        if (Build.VERSION.SDK_INT < 11) // WebInterface requires P2P encryption API11+ EC KeyFactory
            return false;
        return Firebase.isEnabled(context);
    }

    public static String[] permissions(Context context) {
        ArrayList<String> pp = new ArrayList<>();
        if (Storage.isEnabled(context))
            pp.addAll(Arrays.asList(Storage.PERMISSIONS_RW));
        if (IMAPSyncPreferenceCompat.isEnabled(context))
            pp.addAll(Arrays.asList(IMAPSyncPreferenceCompat.PERMISSIONS));
        if (SMSPreferenceCompat.isEnabled(context))
            pp.addAll(Arrays.asList(SMSPreferenceCompat.PERMISSIONS));
        if (CallLogsPreferenceCompat.isEnabled(context))
            pp.addAll(Arrays.asList(CallLogsPreferenceCompat.PERMISSIONS));
        if (from(context).schedules.isEnabled())
            pp.addAll(Arrays.asList(ScheduleService.PERMISSIONS));
        if (DefaultSMSPreferenceCompat.isEnabled(context))
            pp.addAll(Arrays.asList(DefaultSMSPreferenceCompat.PERMISSIONS));
        return pp.toArray(new String[0]);
    }

    public static class BitcoinStorage extends Bitcoin {
        Context context;

        public BitcoinStorage(Context context) {
            this.context = context;
            load();
        }

        public void load() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String sec = prefs.getString(SEC, "");
            if (sec.isEmpty()) {
                generate();
                save();
            } else {
                load(sec);
            }
        }

        public void save() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(SEC, saveSec());
            edit.commit();
        }
    }

    public class ScheduleSMSStorage extends ArrayList<ScheduleSMS> {
        public ScheduleSMSStorage() {
            load();
        }

        public void load() {
            clear();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            int count = prefs.getInt(SCHEDULER_COUNT, 0);
            for (int i = 0; i < count; i++) {
                String state = prefs.getString(SCHEDULER_ITEM + i, "");
                ScheduleSMS item = new ScheduleSMS(SMSApplication.this, state);
                add(item);
            }
        }

        public void save() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt(SCHEDULER_COUNT, size());
            for (int i = 0; i < size(); i++) {
                ScheduleSMS item = get(i);
                edit.putString(SCHEDULER_ITEM + i, item.save().toString());
            }
            edit.commit();
        }

        public boolean isEnabled() {
            TreeSet<Long> events = new TreeSet<>();
            for (ScheduleSMS s : this) {
                if (s.next != 0 && s.enabled)
                    events.add(s.next);
            }
            return !events.isEmpty();
        }

        public PendingIntent registerNextAlarm() {
            TreeSet<Long> events = new TreeSet<>();
            for (ScheduleSMS s : this) {
                if (s.next != 0 && s.enabled)
                    events.add(s.next);
            }

            Calendar cur = Calendar.getInstance();

            Intent alarmIntent = new Intent(SMSApplication.this, ScheduleService.class).setAction(ScheduleService.ACTION_SMS);

            if (events.isEmpty()) {
                AlarmManager.cancel(SMSApplication.this, alarmIntent);
                return null;
            } else {
                long time = events.first();

                alarmIntent.putExtra("time", time);

                Log.d(TAG, "Current: " + ScheduleSMS.formatDate(cur.getTimeInMillis()) + " " + ScheduleSMS.formatTime(cur.getTimeInMillis()) + "; SetAlarm: " + ScheduleSMS.formatDate(time) + " " + ScheduleSMS.formatTime(time));

                return AlarmManager.setExact(SMSApplication.this, time, alarmIntent);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Firebase.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        persistent = new NotificationChannelCompat(this, "status", "Persistent Notifications", NotificationManagerCompat.IMPORTANCE_LOW);
        notifications = new NotificationsStorage(this);

        schedules = new ScheduleSMSStorage();

        IntentFilter ff = new IntentFilter();
        for (String c : COMMANDS)
            ff.addAction(c);
        registerReceiver(receiver, ff);
    }

    public SIM getSIM() {
        if (sim == null)
            sim = new SIM(this);
        return sim;
    }

    public static Uri shareFile(Context context, String type, String name, byte[] buf) {
        try {
            File outputDir = context.getCacheDir();
            File outputFile = File.createTempFile("share", ".tmp", outputDir);
            IOUtils.write(buf, new FileOutputStream(outputFile));
            return FileProvider.getProvider().share(type, name, outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void shareClear(Context context) {
        File outputDir = context.getCacheDir();
        File[] ff = outputDir.listFiles();
        if (ff != null) {
            for (File f : ff) {
                if (f.getName().startsWith("share"))
                    f.delete();
            }
        }
    }

    public static String toHexString(int l) {
        return String.format("%08X", l);
    }

    public static String toHexString(long l) {
        return String.format("%016X", l);
    }

    public boolean wifi() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        t = WifiKeepService.wifi(this, ScheduleService.class, shared.getBoolean(SMSApplication.PREF_WIFIRESTART, false));
        return t != null;
    }
}
