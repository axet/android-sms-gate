package com.github.axet.smsgate.app;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.CallLog;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;

import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.providers.SMS;
import com.zegoggles.smssync.SmsConsts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// API19+ writing / deleting sms requires to be default SMS app. Use DefaultSMSPreferenceCompat
//
//  /data/data/com.android.providers.telephony/databases/mmssms.db
public class SmsStorage {
    public static final String ID = BaseColumns._ID;
    public static final String DESC = "DESC";

    public static final int IN = Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX;
    public static final int OUT = Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT;

    ContentResolver resolver;

    public static HashMap<String, Message> getMessagesFromIntent(SIM sim, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return null;

        Object[] pdusObj = (Object[]) bundle.get("pdus");

        SmsMessage[] messages = new SmsMessage[pdusObj.length];

        for (int i = 0; i < messages.length; i++) {
            if (Build.VERSION.SDK_INT >= 23) {
                String format = intent.getStringExtra("format");
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
            } else {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            }
        }

        int simID = bundle.getInt(SubscriptionManager.EXTRA_SUBSCRIPTION_INDEX, -1);
        if (simID == -1)
            simID = bundle.getInt("subscription", -1);
        if (simID == -1) {
            int slot = bundle.getInt("slot", -1);
            if (sim.getCount() > slot)
                simID = sim.getSimID(slot);
        }

        HashMap<String, Message> map = new HashMap<>();
        for (SmsMessage msg : messages) {
            String id = msg.getOriginatingAddress() + " " + msg.getTimestampMillis();
            Message m = map.get(id);
            if (m != null) {
                m.body += msg.getMessageBody();
            } else {
                Message k = new InboxMessage(msg, simID);
                map.put(id, k);
            }
        }
        return map;
    }

    public static Map<String, String> getMessageMap(Cursor cursor) {
        final String[] columns = cursor.getColumnNames();
        final Map<String, String> map = new HashMap<>(columns.length);
        for (String column : columns) {
            String value;
            try {
                final int index = cursor.getColumnIndex(column);
                if (index != -1) {
                    value = cursor.getString(index);
                } else {
                    continue;
                }
            } catch (SQLiteException ignored) { // this can happen in case of BLOBS in the DB column type checking is API level >= 11
                value = "[BLOB]";
            }
            map.put(column, value);
        }
        return map;
    }

    public static Message getMessage(Cursor cursor) {
        Message m = new Message();
        m.id = cursor.getLong(cursor.getColumnIndex(ID));
        m.type = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.TYPE));
        m.date = cursor.getLong(cursor.getColumnIndex(Telephony.Sms.DATE));
        m.remote_date = cursor.getLong(cursor.getColumnIndex(Telephony.Sms.DATE_SENT));
        m.phone = cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS));
        m.body = cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY));
        m.thread = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.THREAD_ID));
        m.read = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.READ)) == 1;
        m.service = cursor.getString(cursor.getColumnIndex(Telephony.Sms.SERVICE_CENTER));
        if (Build.VERSION.SDK_INT >= 22) {
            int col = cursor.getColumnIndex(Telephony.Sms.SUBSCRIPTION_ID);
            if (col >= 0)
                m.simID = cursor.getInt(col);
        }
        return m;
    }

    public static String querySort(String sort, int off, int max) {
        if (max > 0)
            return sort + " LIMIT " + max + (off >= 0 ? " OFFSET " + off : "");
        else
            return sort;
    }

    public static Message loadMessage(String json) {
        try {
            JSONObject j = new JSONObject(json);
            SmsStorage.Message k = new SmsStorage.Message(j);
            return k;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Message {
        public long id;
        public long date; // incoming = sms received time, outgoing = sms creation date
        public long remote_date; // incoming = sms sent remote creation date, outgoing = 0
        public int type; // Telephony.Sms.TYPE and Telephony.TextBasedSmsColumns.MESSAGE_TYPE_ ...
        public String phone; // Telephony.Sms.ADDRESS
        public String body;
        public int thread; // Telephony.Sms.THREAD_ID
        public boolean read; // seen?
        public String service; // sms service number
        public int simID = SubscriptionManager.INVALID_SUBSCRIPTION_ID; // sim card id

        public Message() {
        }

        public Message(String p, String b) {
            this.phone = p;
            this.body = b;
        }

        public Message(SmsMessage msg) {
            this(msg.getOriginatingAddress(), msg.getMessageBody());
            service = msg.getServiceCenterAddress();
            remote_date = msg.getTimestampMillis();
        }

        public Message(JSONObject j) throws JSONException {
            load(j);
        }

        public void load(JSONObject j) throws JSONException {
            id = j.getLong("id");
            date = j.getLong("date");
            remote_date = j.getLong("remote_date");
            type = j.getInt("type");
            phone = j.getString("phone");
            body = j.getString("body");
            thread = j.getInt("thread");
            read = j.getBoolean("read");
            service = j.getString("service");
            simID = j.getInt("simId");
        }

        public String save() {
            try {
                JSONObject json = new JSONObject();
                json.put("id", id);
                json.put("date", date);
                json.put("remote_date", remote_date);
                json.put("type", type);
                json.put("phone", phone);
                json.put("body", body);
                json.put("thread", thread);
                json.put("read", read);
                json.put("service", service);
                json.put("simId", simID);
                return json.toString();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class InboxMessage extends Message {
        public InboxMessage(SmsMessage msg, int simID) {
            super(msg);
            this.type = IN;
            this.simID = simID;
        }

        public InboxMessage(String p, String b) {
            super(p, b);
            type = IN;
        }

        public InboxMessage(JSONObject j) throws JSONException {
            load(j);
            type = IN;
        }
    }

    public static class SentMessage extends Message {
        public SentMessage(String p, String b) {
            super(p, b);
            type = OUT;
        }

        public SentMessage(String p, String b, Date d) {
            super(p, b);
            type = OUT;
            date = d.getTime();
        }

        public SentMessage(JSONObject j) throws JSONException {
            load(j);
            type = OUT;
        }
    }

    public SmsStorage(Context context) {
        resolver = context.getContentResolver();
    }

    public long add(Message m) { // add sms to phone memory
        long date;
        if (m.date == 0)
            date = System.currentTimeMillis();
        else
            date = m.date;
        ContentValues values = new ContentValues();
        values.put(Telephony.Sms.ADDRESS, m.phone);
        values.put(Telephony.Sms.BODY, m.body);
        Uri uri;
        if (m.type == IN)
            uri = Telephony.Sms.Inbox.CONTENT_URI;
        else if (m.type == OUT)
            uri = Telephony.Sms.Sent.CONTENT_URI;
        else
            throw new RuntimeException("unknown type");
        values.put(Telephony.Sms.TYPE, m.type);
        values.put(Telephony.Sms.PROTOCOL, 0);
        values.put(Telephony.Sms.SERVICE_CENTER, m.service);
        values.put(Telephony.Sms.DATE, date);
        if (Build.VERSION.SDK_INT >= 22)
            values.put(Telephony.Sms.SUBSCRIPTION_ID, m.simID);
        values.put(Telephony.Sms.DATE_SENT, m.remote_date);
        values.put(Telephony.Sms.STATUS, Telephony.TextBasedSmsColumns.STATUS_NONE);
        values.put(Telephony.Sms.THREAD_ID, m.thread);
        values.put(Telephony.Sms.READ, m.read ? 1 : 0);
        Uri id = resolver.insert(uri, values);
        return ContentUris.parseId(id);
    }

    public Cursor query(long date, String sort, int off, int max) {
        return resolver.query(Telephony.Sms.CONTENT_URI, null,
                String.format("%s > ? and ( %s == ? or %s == ?)", Telephony.Sms.DATE, Telephony.Sms.TYPE, Telephony.Sms.TYPE),
                new String[]{String.valueOf(date), String.valueOf(SmsStorage.IN), String.valueOf(SmsStorage.OUT)}, querySort(sort, off, max));
    }

    public void delete(long id) {
        resolver.delete(Uri.parse(Telephony.Sms.CONTENT_URI + "/" + id), null, null);
    }

    public void updateAllThreads() {
        // thread dates + states might be wrong, we need to force a full update
        // unfortunately there's no direct way to do that in the SDK, but passing a
        // negative conversation id to delete should to the trick
        resolver.delete(Uri.parse(Telephony.Sms.Conversations.CONTENT_URI + "/-1"), null, null);
    }

    public boolean exists(long date, String phone, String type) { // just assume equality on date+address+type
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID},
                String.format("%s = ? AND %s = ?", type, Telephony.Sms.ADDRESS),
                new String[]{String.valueOf(date), phone}, null);
        boolean exists = false;
        if (c != null) {
            exists = c.getCount() > 0;
            c.close();
        }
        return exists;
    }

    public ArrayList<Long> deleteThread(int thread) {
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID, Telephony.Sms.THREAD_ID},
                Telephony.Sms.THREAD_ID + " = ?",
                new String[]{Integer.toString(thread)}, null);
        if (c != null) {
            ArrayList<Long> ids = new ArrayList<>();
            while (c.moveToNext()) {
                long id = c.getLong(0);
                long threadId = c.getLong(1);
                if (threadId == thread) {
                    delete(id);
                    ids.add(id);
                }
            }
            c.close();
            if (ids.isEmpty())
                return null;
            return ids;
        }
        return null;
    }

    public ArrayList<Long> deleteOld(long date) {
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID},
                Telephony.Sms.DATE + " < ?",
                new String[]{Long.toString(date)}, null);
        if (c != null) {
            ArrayList<Long> ids = new ArrayList<>();
            while (c.moveToNext()) {
                long id = c.getLong(0);
                delete(id);
                ids.add(id);
            }
            c.close();
            if (ids.isEmpty())
                return null;
            return ids;
        }
        return null;
    }
}
