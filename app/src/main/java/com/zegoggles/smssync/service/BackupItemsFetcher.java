package com.zegoggles.smssync.service;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.provider.Telephony;
import android.util.Log;

import com.github.axet.smsgate.app.CallsStorage;
import com.github.axet.smsgate.app.SmsStorage;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.mail.DataType;

import org.jetbrains.annotations.NotNull;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class BackupItemsFetcher {
    private final Context context;
    private final ContentResolver resolver;
    private final BackupQueryBuilder queryBuilder;
    SmsStorage storage;
    CallsStorage calls;

    public BackupItemsFetcher(@NotNull Context context,
                              @NotNull ContentResolver resolver,
                              @NotNull BackupQueryBuilder queryBuilder) {
        if (resolver == null) throw new IllegalArgumentException("resolver cannot be null");
        if (queryBuilder == null) throw new IllegalArgumentException("queryBuilder cannot be null");

        this.queryBuilder = queryBuilder;
        this.context = context;
        this.resolver = resolver;
        this.storage = new SmsStorage(context);
        this.calls = new CallsStorage(context);
    }

    public
    @NotNull
    Cursor getItemsForDataType(DataType dataType, int max) {
        if (LOCAL_LOGV) Log.v(TAG, "getItemsForDataType(type=" + dataType + ", max=" + max + ")");
        Cursor cursor = null;
        switch(dataType) {
            case SMS:
                cursor = storage.query(queryBuilder.getLastSMS(), Telephony.TextBasedSmsColumns.DATE, 0, max);
                break;
            case CALL:
                cursor = calls.query(queryBuilder.getLastCALL(), Telephony.TextBasedSmsColumns.DATE, 0, max);
                break;
        }
        if (cursor == null)
            return emptyCursor();
        return cursor;
    }

    public long getMostRecentTimestamp(DataType dataType) {
        switch (dataType) {
            default:
                return getMostRecentTimestampForQuery();
        }
    }

    private long getMostRecentTimestampForQuery() {
        Cursor cursor = storage.query(queryBuilder.getLastSMS(), Telephony.TextBasedSmsColumns.DATE + " " + SmsStorage.DESC, 0, 1);
        try {
            if (cursor.moveToFirst()) {
                return cursor.getLong(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.DATE));
            } else {
                return DataType.Defaults.MAX_SYNCED_DATE;
            }
        } finally {
            cursor.close();
        }
    }

    static Cursor emptyCursor() {
        return new MatrixCursor(new String[]{});
    }

    public Context getContext() {
        return context;
    }
}
