package com.github.axet.smsgate.services;

import static com.zegoggles.smssync.service.BackupType.REGULAR;
import static com.zegoggles.smssync.service.BackupType.UNKNOWN;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.PhoneStateChangeListener;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.widgets.CallLogsPreferenceCompat;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.Alarms;
import com.zegoggles.smssync.service.BackupType;
import com.zegoggles.smssync.service.ImapSmsService;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;

// to disable call logging run following:
//
// adb shell "pm disable com.github.axet.smsgate/.services.FileCallsService"
public class FileCallsService extends Service {
    public static final String TAG = FileCallsService.class.getSimpleName();

    public static int NOTIFICATION_ICON = 304;

    OptimizationPreferenceCompat.NotificationIcon icon;
    Storage storage;
    Thread thread;
    Handler handler = new Handler();

    public static boolean isEnabled(Context context) {
        if (!CallLogsPreferenceCompat.isEnabled(context))
            return false;
        return Storage.isEnabled(context);
    }

    public static void start(Context context, Intent intent) {
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleCallsBackup(context);
        CallsService.start(context, intent);
    }

    public static void startIfEnabled(Context context) {
        if (!isEnabled(context))
            return;
        incoming(context, 0);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, FileCallsService.class);
        context.stopService(intent);
        Alarms a = new Alarms(context);
        AlarmManager.cancel(context, a.createPendingIntent(context, UNKNOWN, FileCallsService.class));
    }

    public static long scheduleCallsBackup(Context context) {
        Alarms a = new Alarms(context);
        return a.scheduleBackup(new Preferences(context).getRegularTimeoutSecs(), REGULAR, false, FileCallsService.class);
    }

    public static void incoming(Context context, long last) {
        if (!isEnabled(context))
            return;
        Intent intent = new Intent(context, FileCallsService.class).putExtra("last", last);
        start(context, intent);
    }

    public static void incoming(Context context, boolean skip) {
        if (Storage.getLastCallsDate(context) == 0 && skip)
            Storage.setLastCallsDate(context, System.currentTimeMillis());
        incoming(context, 0);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "FileCallService create");

        icon = new OptimizationPreferenceCompat.NotificationIcon(this, NOTIFICATION_ICON) {
            @Override
            public void updateIcon() {
                updateIcon((Intent) null); // we do not need two icons on low API phones
            }

            @Override
            public boolean isOptimization() {
                return Build.VERSION.SDK_INT >= 26 && context.getApplicationInfo().targetSdkVersion >= 26; // show double icons for API26+
            }

            @Override
            public Notification build(Intent intent) {
                return new OptimizationPreferenceCompat.PersistentIconBuilder(context).setWhen(notification)
                        .create(R.style.AppThemeDark, SMSApplication.from(context).persistent)
                        .setText("FileCallService")
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification).build();
            }
        };
        icon.create();

        storage = new Storage(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isEnabled(this))
            scheduleCallsBackup(this);
        long last = 0;
        if (intent != null)
            last = intent.getLongExtra("last", 0);
        incoming(last);
        return super.onStartCommand(intent, flags, startId);
    }

    public void incoming(final long last) {
        if (thread == null) {
            thread = new Thread("File Calls Thread") {
                @Override
                public void run() {
                    Looper.prepare();
                    try {
                        long old = 1;
                        while (old != 0) {
                            for (int i = 0; i < 10; i++) {
                                if (Storage.isEnabled(FileCallsService.this)) {
                                    old = storage.calls();
                                    if (old < last) {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            Thread.currentThread().interrupt();
                                        }
                                        continue;
                                    }
                                }
                                break;
                            }
                        }
                    } finally {
                        thread = null;
                    }
                }
            };
            thread.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            thread = null;
        }
        icon.close();
        handler.removeCallbacksAndMessages(null);
    }
}
