package com.github.axet.smsgate.services;

import android.Manifest;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.services.WifiKeepService;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.ScheduleSMS;
import com.github.axet.smsgate.app.ScheduleTime;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.dialogs.RebootDialogFragment;
import com.github.axet.smsgate.providers.SMS;

import java.util.ArrayList;
import java.util.Calendar;

public class ScheduleService extends PersistentService implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = ScheduleService.class.getSimpleName();
    public static int NOTIFICATION_ICON = 801;

    public static String[] PERMISSIONS = new String[]{Manifest.permission.SEND_SMS};

    // upcoming noticiation alarm action. triggers notification upcoming.
    public static final String REGISTER = ScheduleService.class.getCanonicalName() + ".REGISTER";
    public static final String ACTION_SMS = ScheduleService.class.getCanonicalName() + ".SMS";

    static {
        OptimizationPreferenceCompat.setEventServiceIcon(true);
    }

    public static boolean registerNext(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SMSApplication app = SMSApplication.from(context);
        boolean n = false;
        n |= app.schedules.registerNextAlarm() != null;
        n |= RebootDialogFragment.schedule(context) != 0;
        n |= app.wifi();
        n |= !shared.getString(SMSApplication.PREF_DELETE, context.getString(R.string.delete_off)).equals(context.getString(R.string.delete_off));
        return OptimizationPreferenceCompat.isPersistent(context, SMSApplication.PREF_OPTIMIZATION, n);
    }

    public static void startIfEnabled(Context context) {
        if (!registerNext(context)) {
            stop(context);
            return;
        }
        OptimizationPreferenceCompat.startService(context, new Intent(context, ScheduleService.class));
    }

    public static void stop(Context context) {
        context.stopService(new Intent(context, ScheduleService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreateOptimization() {
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, NOTIFICATION_ICON, SMSApplication.PREF_OPTIMIZATION, SMSApplication.PREF_NEXT) {
            @Override
            public void register() {
                super.register();
                deleteOld();
            }

            @Override
            public void check() {
                registerNext();
            }

            public Notification build(Intent intent) {
                return new OptimizationPreferenceCompat.PersistentIconBuilder(context).create(R.style.AppThemeDark, SMSApplication.from(context).persistent)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification).build();
            }
        };
        optimization.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStartCommand(Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onStartCommand " + action);
        if (action != null) {
            if (action.equals(REGISTER)) {
                registerNext();
            }
            if (action.equals(ACTION_SMS)) {
                long time = intent.getLongExtra("time", 0);
                sms(time);
            }
            if (action.equals(WifiKeepService.WIFI)) {
                registerNext();
            }
        }
    }

    public void sms(long time) {
        SMSApplication app = SMSApplication.from(this);
        for (ScheduleSMS s : new ArrayList<>(app.schedules)) {
            if (s.next == time && s.enabled) {
                if (s.phone.isEmpty())
                    continue;
                Log.d(TAG, "Send SMS: " + s.phone);
                if (s.sim == -1) {
                    if (s.hide)
                        SMS.send(s.phone, s.message);
                    else
                        SMS.send(this, s.phone, s.message);
                } else {
                    if (s.hide)
                        SMS.send(s.sim, s.phone, s.message);
                    else
                        SMS.send(this, s.sim, s.phone, s.message);
                }
                if (s.repeat == ScheduleTime.REPEAT_DELETE)
                    app.schedules.remove(s);
                else
                    s.fired();
            }
        }
        app.schedules.save();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        registerNext();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void registerNext() {
        if (!registerNext(this))
            stopSelf();
    }

    void deleteOld() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        String d = shared.getString(SMSApplication.PREF_DELETE, SMSApplication.PREF_DELETE_OFF);
        if (d.equals(SMSApplication.PREF_DELETE_OFF))
            return;

        Calendar c = Calendar.getInstance();

        if (d.equals(SMSApplication.PREF_DELETE_1DAY))
            c.add(Calendar.DAY_OF_YEAR, -1);
        if (d.equals(SMSApplication.PREF_DELETE_1WEEK))
            c.add(Calendar.WEEK_OF_YEAR, -1);
        if (d.equals(SMSApplication.PREF_DELETE_1MONTH))
            c.add(Calendar.MONTH, -1);
        if (d.equals(SMSApplication.PREF_DELETE_3MONTH))
            c.add(Calendar.MONTH, -3);
        if (d.equals(SMSApplication.PREF_DELETE_6MONTH))
            c.add(Calendar.MONTH, -6);

        SmsStorage storage = new SmsStorage(this);
        storage.deleteOld(c.getTimeInMillis());
    }
}
