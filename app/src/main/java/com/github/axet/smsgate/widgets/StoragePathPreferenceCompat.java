package com.github.axet.smsgate.widgets;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.widget.Toast;

import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.OpenStorageChoicer;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.services.FileReplyService;

import java.io.File;

public class StoragePathPreferenceCompat extends com.github.axet.androidlibrary.preferences.StoragePathPreferenceCompat {
    CharSequence defSummary;

    public static boolean isVisible(Context context) { // hide SMS features from Tables without tehephony
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    public StoragePathPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StoragePathPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StoragePathPreferenceCompat(Context context) {
        super(context);
    }

    public void create() {
        defSummary = getSummary();
        choicer = new OpenStorageChoicer(storage, OpenFileDialog.DIALOG_TYPE.FOLDER_DIALOG, false) {
            @Override
            public void onResult(Uri uri) {
                if (callChangeListener(uri.toString()))
                    setText(uri.toString());
                FileReplyService.start(getContext());
            }

            @Override
            public OpenFileDialog fileDialogBuild() {
                OpenFileDialog d = super.fileDialogBuild();
                d.setNeutralButton(context.getString(R.string.reset_path_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(R.string.disable_sms_storage);
                        builder.setMessage(R.string.are_you_sure);
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog2, int which) {
                                reset();
                                dialog.dismiss();
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ;
                            }
                        });
                        builder.show();
                    }
                });
                return d;
            }
        };
        choicer.setTitle(getTitle().toString());
        choicer.setContext(getContext());

        if (!isVisible(getContext()))
            setVisible(false);
    }

    public void reset() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(getKey());
        editor.apply();
        setSummary(defSummary);
        FileReplyService.stop(getContext());
    }

    @Override
    public void onClick() { // empty, use setOnPreferenceClickListener and premission checks
    }

    @Override
    public boolean onLongClick() { // SAF has no custom buttons, acting on reset button
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.disable_sms_storage);
        builder.setMessage(R.string.are_you_sure);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                reset();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ;
            }
        });
        builder.show();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(permissions, grantResults);
    }

    @Override
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) { // allow to show null
        String v = restoreValue ? getPersistedString(getText()) : (String) defaultValue;
        Uri u = storage.getStoragePath(v);
        if (u != null)
            setSummary(Storage.getDisplayName(getContext(), u));
    }

    @Override
    public Object onGetDefaultValue(TypedArray a, int index) {
        super.onGetDefaultValue(a, index);
        return null; // no default for books reader
    }
}
