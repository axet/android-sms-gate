package com.github.axet.smsgate.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.AttributeSet;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.fragments.SettingsFragment;
import com.github.axet.smsgate.services.CallsService;
import com.github.axet.smsgate.services.FileCallsService;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.ImapSmsService;

public class CallLogsPreferenceCompat extends SwitchPreferenceCompat {
    public static final String TAG = CallLogsPreferenceCompat.class.getSimpleName();

    public static String[] PERMISSIONS = new String[]{Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS};

    public Fragment f;

    public static boolean isEnabled(Context context) {
        if (!Storage.permitted(context, PERMISSIONS))
            return false;
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getBoolean(SMSApplication.PREF_CALLLOGS, true);
    }

    @TargetApi(21)
    public CallLogsPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public CallLogsPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public CallLogsPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public CallLogsPreferenceCompat(Context context) {
        super(context);
        create();
    }

    void create() {
        onResume();
    }

    @Override
    protected void onClick() {
        if (!Storage.permitted(f, PERMISSIONS, SMSGateFragment.RESULT_CALLS))
            return;
        super.onClick();
        if (isChecked()) {
            FileCallsService.startIfEnabled(getContext());
            ImapSmsService.startIfEnabled(getContext());
        } else {
            FileCallsService.stop(getContext());
            CallsService.stop(getContext());
        }
    }

    public void onResume() {
        if (isChecked())
            setChecked(isEnabled(getContext()));
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (Storage.permitted(getContext(), permissions))
            onClick();
        else
            setChecked(false);
    }
}
