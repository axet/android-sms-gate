package com.github.axet.smsgate.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.role.RoleManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Telephony;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.services.FirebaseService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class DefaultSMSPreferenceCompat extends SwitchPreferenceCompat {
    public static final String TAG = DefaultSMSPreferenceCompat.class.getSimpleName();

    public static final String WRITE_SMS = "android.permission.WRITE_SMS";

    public static String[] PERMISSIONS = new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS, WRITE_SMS, Manifest.permission.SEND_SMS};

    public Activity a;

    public static boolean isEnabled(Context context) {
        final String pkg = context.getPackageName();
        if (Build.VERSION.SDK_INT >= 29) {
            RoleManager rm = context.getSystemService(RoleManager.class);
            if (rm.isRoleAvailable(RoleManager.ROLE_SMS))
                return rm.isRoleHeld(RoleManager.ROLE_SMS);
            return false;
        } else if (Build.VERSION.SDK_INT >= 19) {
            return Telephony.Sms.getDefaultSmsPackage(context).equals(pkg);
        }
        return false;
    }

    @TargetApi(19)
    public static void show(Activity a) {
        final Context context = a;
        final String pkg = context.getPackageName();
        if (Build.VERSION.SDK_INT >= 29) {
            RoleManager rm = context.getSystemService(RoleManager.class);
            if (rm.isRoleAvailable(RoleManager.ROLE_SMS)) {
                if (rm.isRoleHeld(RoleManager.ROLE_SMS)) {
                    Toast.Text(context, "Can't remove roles");
                } else {
                    Intent intent = rm.createRequestRoleIntent(RoleManager.ROLE_SMS);
                    a.startActivityForResult(intent, 0);
                }
            }
        } else {
            if (Telephony.Sms.getDefaultSmsPackage(context).equals(pkg)) {
                final SMSAdapter adapter = new SMSAdapter(context);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Change SMS app?");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, adapter.getItem(which).packageName);
                        context.startActivity(intent);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
            } else {
                Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, pkg);
                context.startActivity(intent);
            }
        }
    }

    public static class SMSAdapter implements ListAdapter {
        Context context;
        ArrayList<ApplicationInfo> packages;

        public SMSAdapter(Context context) {
            this.context = context;
            final PackageManager pm = context.getPackageManager();
            Set<String> ss = new TreeSet<>();
            packages = new ArrayList<>();
            Intent i = new Intent();
            i.setAction(Telephony.Sms.Intents.SMS_DELIVER_ACTION);
            final List<ResolveInfo> rr = pm.queryBroadcastReceivers(i, PackageManager.GET_RESOLVED_FILTER);
            for (ResolveInfo r : rr) {
                if (!ss.contains(r.activityInfo.packageName)) {
                    ss.add(r.activityInfo.packageName);
                    packages.add(r.activityInfo.applicationInfo);
                }
            }
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public int getCount() {
            return packages.size();
        }

        @Override
        public ApplicationInfo getItem(int position) {
            return packages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @TargetApi(19)
        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.application_item, parent, false);
            }
            final ApplicationInfo info = packages.get(position);
            PackageManager pm = context.getPackageManager();
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            TextView text = (TextView) convertView.findViewById(R.id.item_text);
            TextView sum = (TextView) convertView.findViewById(R.id.item_summary);
            SwitchCompat sw = (SwitchCompat) convertView.findViewById(R.id.item_switch);
            sw.setClickable(false);

            String n = FirebaseService.getApplicationName(context, info);
            Drawable d = info.loadIcon(pm);

            icon.setImageDrawable(d);
            text.setText(n);
            sum.setText(info.packageName);

            String dd = Telephony.Sms.getDefaultSmsPackage(context);
            sw.setChecked(dd.equals(info.packageName));

            // forcing on click (not working otherwise!?)
            final View c = convertView;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListView ll = (ListView) parent;
                    ll.getOnItemClickListener().onItemClick(ll, c, position, getItemId(position));
                }
            });

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return packages.size() == 0;
        }
    }

    @TargetApi(21)
    public DefaultSMSPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public DefaultSMSPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public DefaultSMSPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public DefaultSMSPreferenceCompat(Context context) {
        super(context);
        create();
    }

    void create() {
        onResume();
    }

    public void onResume() {
        if (!OptimizationPreferenceCompat.findPermissions(getContext(), PERMISSIONS)) {
            setVisible(false);
            return;
        }
        final Context context = getContext();
        final String n = context.getPackageName();
        if (Build.VERSION.SDK_INT < 19 || (Build.VERSION.SDK_INT >= 29 && a == null)) {
            setVisible(false);
        } else {
            setVisible(true);
            String d = Telephony.Sms.getDefaultSmsPackage(context);
            if (d == null) { // disabled, no phone support
                setVisible(false);
                return;
            }
            String dd = FirebaseService.getApplicationName(context, d);
            setSummary(dd);
            setChecked(d.equals(n));
            setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                @TargetApi(19)
                public boolean onPreferenceChange(Preference preference, Object o) {
                    show(a);
                    return false;
                }
            });
        }
    }

}
