package com.zegoggles.smssync.service;

import static com.zegoggles.smssync.mail.DataType.MMS;
import static com.zegoggles.smssync.mail.DataType.CALL;
import static com.zegoggles.smssync.mail.DataType.SMS;

import android.content.Context;
import android.net.Uri;

import com.zegoggles.smssync.SmsConsts;

public class BackupQueryBuilder {
    private final Context context;

    public BackupQueryBuilder(Context context) {
        this.context = context;
    }

    public static class Query {
        final Uri uri;
        final String[] projection;
        final String selection;
        final String[] selectionArgs;
        final String sortOrder;

        public Query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
            this.uri = uri;
            this.projection = projection;
            this.selection = selection;
            this.selectionArgs = selectionArgs;
            this.sortOrder = sortOrder;
        }

        public Query(Uri uri, String[] projection, String selection, String[] selectionArgs, int max) {
            this(uri, projection, selection, selectionArgs,
                    max > 0 ? SmsConsts.DATE + " LIMIT " + max : SmsConsts.DATE);
        }
    }

    public long getLastSMS() {
        return SMS.getMaxSyncedDate(context);
    }

    public long getLastCALL() {
        return CALL.getMaxSyncedDate(context);
    }

    public long getLastMMS() {
        long maxSynced = MMS.getMaxSyncedDate(context);
        if (maxSynced > 0) {
            // NB: max synced date is stored in seconds since epoch in database
            maxSynced = (long) (maxSynced / 1000d);
        }
        return maxSynced;
    }
}
