package com.github.axet.smsgate.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.view.accessibility.AccessibilityEvent;

import com.github.axet.smsgate.app.SMSApplication;
import com.zegoggles.smssync.activity.SMSGateFragment;

public class NotificationService extends AccessibilityService {
    public static String TAG = NotificationService.class.getSimpleName();

    public static String ADD = "ADD";
    public static String REMOVE = "REMOVE";
    public static String UPDATE = "UPDATE";

    public static void start(Context context) {
        Intent intent = new Intent(context, NotificationService.class);
        context.startService(intent);
    }

    public static boolean enabled(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        String packageName = NotificationService.class.getCanonicalName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    public static void show(Context context) {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        context.startActivity(intent);
    }

    public static void startIfEnabled(Context context) {
        if (enabled(context))
            start(context);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
            Notification n = (Notification) event.getParcelableData();
            if (n != null) {
                String id = SMSApplication.toHexString(event.getPackageName().toString().hashCode());
                SMSApplication.from(this).notifications.notification(NotificationService.UPDATE, FirebaseService.toString(event.getPackageName()), id, n);
                SMSGateFragment.checkPermissions(this);
                FirebaseService.notification(this, NotificationService.UPDATE, FirebaseService.toString(event.getPackageName()), id, n);
            }
        }
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }
}