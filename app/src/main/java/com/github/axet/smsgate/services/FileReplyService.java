package com.github.axet.smsgate.services;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.widgets.SMSPreferenceCompat;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.Alarms;

import static com.zegoggles.smssync.service.BackupType.REPLY;
import static com.zegoggles.smssync.service.BackupType.UNKNOWN;

// for disabling fire reply rervice run:
//
// adb shell "pm disable com.github.axet.smsgate/.services.FileReplyService"
public class FileReplyService extends Service {
    public static final String TAG = FileReplyService.class.getSimpleName();

    public static int NOTIFICATION_ICON = 402;

    OptimizationPreferenceCompat.NotificationIcon icon;
    Storage storage;
    Thread thread;

    public static void start(Context context) {
        Intent intent = new Intent(context, FileReplyService.class);
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleReplyBackup(context);
    }

    public static void startIfEnabled(Context context) {
        if (!Storage.isEnabled(context))
            return;
        if (!SMSPreferenceCompat.isEnabled(context))
            return;
        start(context);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, FileReplyService.class);
        context.stopService(intent);
        Alarms a = new Alarms(context);
        AlarmManager.cancel(context, a.createPendingIntent(context, UNKNOWN, FileReplyService.class));
    }

    public static long scheduleReplyBackup(Context context) {
        Alarms a = new Alarms(context);
        return a.scheduleBackup(new Preferences(context).getRegularTimeoutSecs(), REPLY, false, FileReplyService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "create");

        icon = new OptimizationPreferenceCompat.NotificationIcon(this, NOTIFICATION_ICON) {
            @Override
            public void updateIcon() {
                updateIcon((Intent) null); // we do not need two icons on low API phones
            }

            @Override
            public boolean isOptimization() {
                return Build.VERSION.SDK_INT >= 26 && context.getApplicationInfo().targetSdkVersion >= 26; // show double icons for API26+
            }

            @Override
            public Notification build(Intent intent) {
                return new OptimizationPreferenceCompat.PersistentIconBuilder(context).setWhen(notification)
                        .create(R.style.AppThemeDark, SMSApplication.from(context).persistent)
                        .setText(TAG)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification).build();
            }
        };
        icon.create();

        storage = new Storage(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        scheduleReplyBackup(this);
        if (thread == null) {
            thread = new Thread("Reply Thread") {
                @Override
                public void run() {
                    try {
                        storage.reply();
                    } finally {
                        thread = null;
                    }
                }
            };
            thread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            thread = null;
        }
        icon.close();
    }
}
