
## MediaTek Docs

  * http://git.huayusoft.com/tomsu/AP7200_MDK-kernel/blob/master/mediatek/frameworks/api/1.txt

```java
public class SmsManagerEx {
    method public java.util.ArrayList<java.lang.String> divideMessage(java.lang.String);
    method public static com.mediatek.telephony.SmsManagerEx getDefault();
    method public void sendDataMessage(java.lang.String, java.lang.String, short, byte[], android.app.PendingIntent, android.app.PendingIntent, int);
    method public void sendDataMessage(java.lang.String, java.lang.String, short, short, byte[], android.app.PendingIntent, android.app.PendingIntent, int);
    method public void sendMultipartTextMessage(java.lang.String, java.lang.String, java.util.ArrayList<java.lang.String>, java.util.ArrayList<android.app.PendingIntent>, java.util.ArrayList<android.app.PendingIntent>, int);
    method public void sendMultipartTextMessageWithExtraParams(java.lang.String, java.lang.String, java.util.ArrayList<java.lang.String>, android.os.Bundle, java.util.ArrayList<android.app.PendingIntent>, java.util.ArrayList<android.app.PendingIntent>, int);
    method public void sendTextMessage(java.lang.String, java.lang.String, java.lang.String, android.app.PendingIntent, android.app.PendingIntent, int);
    method public void sendTextMessageWithExtraParams(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle, android.app.PendingIntent, android.app.PendingIntent, int);
  }
```

  * [MediaTek_SDK_for_Android_Developers_Guide_v1_0.pdf](http://labs.mediatek.com/fileMedia/download/5f1471ad-ff1d-4fea-9828-0bc69c63b3e3)

```java

// Send this multi-part text to target.
try {
  SmsManagerEx.getDefault().sendMultipartTextMessageWithEncodingType( destAddr/* targeAddress */, scAddr/* serviceCenter*/, messages, codingType, sentIntents, deliveryIntents, slotId);
} catch (Exception ex) {
  throw new MmsException("SmsMessageSender.sendMessage: caught " + ex + " from SmsManager.sendTextMessage()");
}
```
