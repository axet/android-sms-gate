package com.github.axet.smsgate.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.AttributeSet;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.services.CallsService;
import com.github.axet.smsgate.services.FileCallsService;
import com.github.axet.smsgate.services.FileReplyService;
import com.github.axet.smsgate.services.FileSmsService;
import com.github.axet.smsgate.services.ImapSmsReplyService;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.ImapSmsService;

public class SMSPreferenceCompat extends SwitchPreferenceCompat {
    public static final String TAG = SMSPreferenceCompat.class.getSimpleName();

    public static String[] PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS};

    public Fragment f;

    public static boolean isEnabled(Context context) {
        if (!Storage.permitted(context, PERMISSIONS))
            return false;
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getBoolean(SMSApplication.PREF_SMSSYNC, true);
    }

    @TargetApi(21)
    public SMSPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public SMSPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public SMSPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public SMSPreferenceCompat(Context context) {
        super(context);
        create();
    }

    void create() {
        onResume();
    }

    @Override
    protected void onClick() {
        if (!Storage.permitted(f, PERMISSIONS, SMSGateFragment.RESULT_SMS))
            return;
        super.onClick();
        if (isChecked()) {
            FileSmsService.startIfEnabled(getContext());
            ImapSmsService.startIfEnabled(getContext());
            ImapSmsReplyService.startIfEnabled(getContext());
        } else {
            FileSmsService.stop(getContext());
            FileReplyService.stop(getContext());
            ImapSmsReplyService.stop(getContext());
        }
    }

    public void onResume() {
        if (isChecked())
            setChecked(isEnabled(getContext()));
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (Storage.permitted(getContext(), permissions))
            onClick();
        else
            setChecked(false);
    }
}
