/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zegoggles.smssync.service;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.BinaryTempFileBody;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.services.CallsService;
import com.github.axet.smsgate.services.NotificationService;
import com.github.axet.smsgate.widgets.CallLogsPreferenceCompat;
import com.github.axet.smsgate.widgets.SMSPreferenceCompat;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.mail.BackupImapStore;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.exception.BackupDisabledException;
import com.zegoggles.smssync.service.exception.ConnectivityException;
import com.zegoggles.smssync.service.exception.NoConnectionException;
import com.zegoggles.smssync.service.exception.RequiresBackgroundDataException;
import com.zegoggles.smssync.service.exception.RequiresLoginException;
import com.zegoggles.smssync.service.exception.RequiresWifiException;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;
import com.zegoggles.smssync.utils.AppLog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;
import java.util.EnumSet;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.service.BackupType.BROADCAST_INTENT;
import static com.zegoggles.smssync.service.BackupType.INCOMING;
import static com.zegoggles.smssync.service.BackupType.MANUAL;
import static com.zegoggles.smssync.service.BackupType.REGULAR;
import static com.zegoggles.smssync.service.BackupType.UNKNOWN;
import static com.zegoggles.smssync.service.state.SmsSyncState.ERROR;
import static com.zegoggles.smssync.service.state.SmsSyncState.FINISHED_BACKUP;
import static com.zegoggles.smssync.service.state.SmsSyncState.INITIAL;

public class ImapSmsService extends ServiceBase {
    public static String TAG = ImapSmsService.class.getSimpleName();

    @Nullable
    private static ImapSmsService service;
    @NotNull
    private BackupState mState = new BackupState();
    Handler handler;

    public static void startIfEnabled(Context context) {
        if (shouldSchedule(context))
            start(context);
    }

    public static void start(Context context) {
        final Intent intent = new Intent(context, ImapSmsService.class);
        intent.putExtra(BackupType.EXTRA, BackupType.REGULAR.name());
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleRegularBackup(context);
        CallsService.startIfEnabled(context);
    }

    public static void startManual(Context context, boolean skip) {
        final Intent intent = new Intent(context, ImapSmsService.class);
        if (new Preferences(context).isFirstBackup())
            intent.putExtra(Consts.KEY_SKIP_MESSAGES, skip);
        // start backup service
        intent.putExtra(BackupType.EXTRA, BackupType.MANUAL.name());
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleRegularBackup(context);
    }

    public static void stop(Context context) {
        Alarms a = new Alarms(context);
        AlarmManager.cancel(context, a.createPendingIntent(context, UNKNOWN, ImapSmsService.class));
    }

    public static long scheduleIncomingBackup(Context context, int t) {
        return getAlarms(context).scheduleBackup(t, INCOMING, false, ImapSmsService.class);
    }

    public static long scheduleIncomingBackup(Context context) {
        if (!shouldSchedule(context))
            return -1;
        return getAlarms(context).scheduleBackup(getPreferences(context).getIncomingTimeoutSecs(), INCOMING, false, ImapSmsService.class);
    }

    public static long scheduleRegularBackup(Context context) {
        return getAlarms(context).scheduleBackup(getPreferences(context).getRegularTimeoutSecs(), REGULAR, false, ImapSmsService.class);
    }

    public static long scheduleBootupBackup(Context context) {
        if (!shouldSchedule(context))
            return -1;
        return getAlarms(context).scheduleBackup(Alarms.BOOT_BACKUP_DELAY, REGULAR, false, ImapSmsService.class);
    }

    public static long scheduleImmediateBackup(Context context) {
        return getAlarms(context).scheduleBackup(-1, BROADCAST_INTENT, true, ImapSmsService.class);
    }

    public static long scheduleBackup(Context context, int delay) {
        return getAlarms(context).scheduleBackup(delay, BROADCAST_INTENT, true, ImapSmsService.class);
    }

    static protected Alarms getAlarms(Context context) {
        return new Alarms(context);
    }

    static protected Preferences getPreferences(Context context) {
        return new Preferences(context);
    }

    static protected AuthPreferences getAuthPreferences(Context context) {
        return new AuthPreferences(context);
    }

    static private void log(Context context, String message, boolean appLog) {
        Log.d(TAG, message);
        if (appLog) {
            new AppLog(DateFormat.getDateFormatOrder(context))
                    .appendAndClose(message);
        }
    }

    public static boolean shouldSchedule(Context context) {
        if (!SMSPreferenceCompat.isEnabled(context) &&
                !CallLogsPreferenceCompat.isEnabled(context) &&
                !NotificationService.enabled(context))
            return false;

        final Preferences preferences = getPreferences(context);

        final boolean autoSync = preferences.isEnableAutoSync();
        final boolean loginInformationSet = getAuthPreferences(context).isLoginInformationSet();
        final boolean firstBackup = preferences.isFirstBackup();
        return autoSync && loginInformationSet && !firstBackup;
    }

    @Override
    @NotNull
    public BackupState getState() {
        return mState;
    }

    public static void clearTempFiles(File tmp) {
        if (tmp == null)
            return;
        File[] ff = tmp.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.toLowerCase().endsWith("tmp"))
                    return true;
                return false;
            }
        });
        if (ff == null)
            return;
        long now = System.currentTimeMillis();
        long old = now - 10 * 60 * 1000; // delete files older then 10 mins
        for (File f : ff) {
            if (f.lastModified() < old)
                f.delete();
        }
    }

    public static void clearTempFiles(Context context) {
        clearTempFiles(context.getCacheDir());
        clearTempFiles(context.getExternalCacheDir());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        service = this;

        ImapSmsService.clearTempFiles(this);
        BinaryTempFileBody.setTempDirectory(getCacheDir());

        start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LOCAL_LOGV) Log.v(TAG, "SmsBackupService#onDestroy(state=" + getState() + ")");
        getNotifier().cancel(BACKUP_ID);
        service = null;
        stop();
    }

    synchronized void start() {
        if (handler != null)
            return;

        handler = new Handler();

        stickToast();
    }

    synchronized void stickToast() {
        if (handler == null)
            return;

        Log.d(TAG, "ShowToast");
        showToast();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stickToast();
            }
        }, 3000);
    }

    synchronized void stop() {
        handler = null;
    }

    void showToast() {
        notifyAboutBackup(mState);
    }

    @Override
    protected void handleIntent(final Intent intent) {
        start();

        // in case if app get killed. schedule next
        scheduleNextBackup();

        if (intent == null) return; // NB: should not happen with START_NOT_STICKY
        final BackupType backupType = BackupType.fromIntent(intent);
        if (LOCAL_LOGV) Log.v(TAG, "handleIntent(" + intent +
                ", " + (intent.getExtras() == null ? "null" : intent.getExtras().keySet()) +
                ", type=" + backupType + ")");

        appLog(R.string.app_log_backup_requested, getString(backupType.resId));

        // Only start a backup if there's no other operation going on at this time.
        if (!isWorking()) {
            backup(backupType, intent.getBooleanExtra(Consts.KEY_SKIP_MESSAGES, false));
        } else {
            appLog(R.string.app_log_skip_backup_already_running);
        }

        stop();
    }

    private void backup(BackupType backupType, boolean skip) {
        try {
            // set initial state
            mState = new BackupState(INITIAL, 0, 0, backupType, null, null);
            EnumSet<DataType> enabledTypes = getEnabledBackupTypes();
            if (!skip) {
                checkCredentials();
                checkBackgroundDataSettings(backupType);
                checkConnectivity();
            }

            appLog(R.string.app_log_start_backup, backupType);

            ImapSmsService.clearTempFiles(this);
            getBackupTask().execute(getBackupConfig(backupType, enabledTypes, getBackupImapStore(), skip));
        } catch (MessagingException e) {
            Log.w(TAG, e);
            moveToState(mState.transition(ERROR, e));
        } catch (RequiresBackgroundDataException e) {
            Log.w(TAG, e);
            moveToState(mState.transition(ERROR, e));
        } catch (ConnectivityException e) {
            Log.w(TAG, e);
            moveToState(mState.transition(ERROR, e));
        } catch (RequiresLoginException e) {
            Log.w(TAG, e);
            appLog(R.string.app_log_missing_credentials);
            moveToState(mState.transition(ERROR, e));
        } catch (BackupDisabledException e) {
            Log.w(TAG, e);
            moveToState(mState.transition(FINISHED_BACKUP, e));
        }
    }

    private BackupConfig getBackupConfig(BackupType backupType,
                                         EnumSet<DataType> enabledTypes,
                                         BackupImapStore imapStore,
                                         boolean skip) {
        return new BackupConfig(
                imapStore,
                0,
                skip,
                getPreferences().getMaxItemsPerSync(),
                backupType,
                enabledTypes,
                getPreferences().isAppLogDebug()
        );
    }

    private EnumSet<DataType> getEnabledBackupTypes() throws BackupDisabledException {
        EnumSet<DataType> dataTypes = DataType.enabled(this);
        if (dataTypes.isEmpty()) {
            throw new BackupDisabledException();
        }
        return dataTypes;
    }

    private void checkCredentials() throws RequiresLoginException {
        if (!getAuthPreferences().isLoginInformationSet()) {
            throw new RequiresLoginException();
        }
    }

    private void checkBackgroundDataSettings(BackupType backupType) throws RequiresBackgroundDataException {
        if (backupType.isBackground() && !getConnectivityManager().getBackgroundDataSetting()) {
            throw new RequiresBackgroundDataException();
        }
    }

    private void checkConnectivity() throws ConnectivityException {
        NetworkInfo active = getConnectivityManager().getActiveNetworkInfo();
        if (active == null || !active.isConnectedOrConnecting()) {
            throw new NoConnectionException();
        }
        if (getPreferences().isWifiOnly() && isBackgroundTask() && !isConnectedViaWifi()) {
            throw new RequiresWifiException();
        }
    }

    protected ImapBackupTask getBackupTask() {
        return new ImapBackupTask(this);
    }

    private void moveToState(BackupState state) {
        backupStateChanged(state);
        App.bus.post(state);
    }

    @Override
    protected boolean isBackgroundTask() {
        return mState.backupType.isBackground();
    }

    @Produce
    public BackupState produceLastState() {
        return mState;
    }

    @Subscribe
    public void backupStateChanged(BackupState state) {
        if (mState == state) return;

        mState = state;
        if (mState.isInitialState()) return;

        if (state.isError()) {
            handleErrorState(state);
        }

        if (state.isRunning()) {
            if (state.backupType == MANUAL) {
                notifyAboutBackup(state);
            }
        } else {
            appLogDebug(state.toString());
            appLog(state.isCanceled() ? R.string.app_log_backup_canceled : R.string.app_log_backup_finished);

            // even after manual backup operation we shall update regular
            // schedule backup, it maybe first run.
            //
            if (state.backupType == BackupType.INCOMING && state.currentSyncedItems == 0) {
                Log.d(TAG, "re-scheduling incoming backup");
                scheduleBackup(this, 5);
            } else {
                Log.d(TAG, "scheduling next backup");
                scheduleNextBackup();
            }
            stopForeground(true);
            stopSelf();
        }
    }

    private void handleErrorState(BackupState state) {
        if (state.isAuthException()) {
            appLog(R.string.app_log_backup_failed_authentication, state.getDetailedErrorMessage(getResources()));

            if (shouldNotifyUser(state)) {
                notifyUser(android.R.drawable.stat_sys_warning,
                        getString(R.string.notification_auth_failure),
                        getString(getAuthPreferences().useXOAuth() ? R.string.status_auth_failure_details_xoauth : R.string.status_auth_failure_details_plain));
            }
        } else if (state.isConnectivityError()) {
            appLog(R.string.app_log_backup_failed_connectivity, state.getDetailedErrorMessage(getResources()));
        } else {
            appLog(R.string.app_log_backup_failed_general_error, state.getDetailedErrorMessage(getResources()));

            if (shouldNotifyUser(state)) {
                notifyUser(android.R.drawable.stat_sys_warning,
                        getString(R.string.notification_general_error),
                        state.getErrorMessage(getResources()));
            }
        }
    }

    private boolean shouldNotifyUser(BackupState state) {
        return state.backupType == MANUAL ||
                (getPreferences().isNotificationEnabled() && !state.isConnectivityError());
    }

    private void notifyAboutBackup(BackupState state) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher_notification);
        builder.setTicker(getString(R.string.status_backup));
        builder.setWhen(System.currentTimeMillis());
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT >= 16)
            builder.setPriority(Notification.PRIORITY_MIN);
        builder.setContentIntent(getPendingIntent());
        builder.setContentTitle(getString(R.string.status_backup));
        builder.setContentText(state.getNotificationLabel(getResources()));
        NotificationChannelCompat channel = new NotificationChannelCompat(this, "status", TAG, NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(builder);
        notification = builder.build();
        NotificationChannelCompat.setChannelId(notification, channel.channelId);
        startForeground(BACKUP_ID, notification);
    }

    private void scheduleNextBackup() {
        final long nextSync = scheduleRegularBackup(this);
        if (nextSync >= 0) {
            appLog(R.string.app_log_scheduled_next_sync,
                    DateFormat.format("kk:mm", new Date(nextSync)));
        } else {
            appLog(R.string.app_log_no_next_sync);
        }
    }

    protected void notifyUser(int icon, String title, String text) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(icon);
        builder.setTicker(getString(R.string.app_name));
        builder.setWhen(System.currentTimeMillis());
        builder.setOnlyAlertOnce(true);
        builder.setAutoCancel(true);
        builder.setContentIntent(getPendingIntent());
        builder.setContentTitle(title);
        builder.setContentText(text);
        NotificationChannelCompat channel = new NotificationChannelCompat(this, "error", TAG + "Error", NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(builder);
        Notification n = builder.build();
        NotificationChannelCompat.setChannelId(n, channel.channelId);
        getNotifier().notify(BACKUP_USER, n);
    }

    public static boolean isServiceWorking() {
        return service != null && service.isWorking();
    }

    public BackupState transition(SmsSyncState newState, Exception e) {
        return mState.transition(newState, e);
    }
}
