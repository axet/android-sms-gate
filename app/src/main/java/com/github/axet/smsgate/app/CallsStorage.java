package com.github.axet.smsgate.app;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.Telephony;

import org.json.JSONException;
import org.json.JSONObject;

//  /data/data/com.android.providers.contacts/databases/calllog.db
public class CallsStorage {
    public static Uri CALLS = Uri.parse("content://call_log/calls");

    public static final int IN = CallLog.Calls.INCOMING_TYPE;
    public static final int OUT = CallLog.Calls.OUTGOING_TYPE;
    public static final int MISS = CallLog.Calls.MISSED_TYPE;

    ContentResolver resolver;

    public static class Call {
        public long id;
        public long date; // incoming = sms received time, outgoing = sms creation date
        public int type; // Telephony.Sms.TYPE and Telephony.TextBasedSmsColumns.MESSAGE_TYPE_ ...
        public boolean missed;
        public String phone; // Telephony.Sms.ADDRESS
        public long duration; // ms
        public String simID; // phone account id

        public Call() {
        }

        public Call(String p) {
            this.phone = p;
        }

        public Call(JSONObject j) throws JSONException {
            load(j);
        }

        public void load(JSONObject j) throws JSONException {
            id = j.getLong("id");
            date = j.getLong("date");
            type = j.getInt("type");
            missed = j.getBoolean("missed");
            phone = j.getString("phone");
            duration = j.getLong("duration");
            simID = j.getString("simID");
        }

        public String save() {
            try {
                JSONObject json = new JSONObject();
                json.put("id", id);
                json.put("date", date);
                json.put("type", type);
                json.put("missed", missed);
                json.put("phone", phone);
                json.put("duration", duration);
                json.put("simID", simID);
                return json.toString();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public CallsStorage(Context context) {
        resolver = context.getContentResolver();
    }

    public Cursor query(long date, String sort, int off, int max) {
        return resolver.query(CALLS, null,
                String.format("%s > ?", Telephony.Sms.DATE),
                new String[]{String.valueOf(date)}, SmsStorage.querySort(sort, off, max));
    }

    public static Call getCall(Cursor cursor) {
        Call m = new Call();
        m.id = cursor.getLong(cursor.getColumnIndex(SmsStorage.ID));
        m.type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
        if (m.type == MISS) {
            m.type = IN;
            m.missed = true;
        }
        m.date = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
        m.phone = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
        m.duration = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DURATION)) * 1000;
        if (Build.VERSION.SDK_INT >= 21)
            m.simID = cursor.getString(cursor.getColumnIndex(CallLog.Calls.PHONE_ACCOUNT_ID));
        return m;
    }
}
