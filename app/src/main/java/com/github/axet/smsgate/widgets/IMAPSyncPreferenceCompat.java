package com.github.axet.smsgate.widgets;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fsck.k9.mail.AuthType;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.services.ImapSmsReplyService;
import com.github.axet.smsgate.services.OnBootReceiver;
import com.zegoggles.smssync.preferences.AuthMode;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.ServerPreferences;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static com.zegoggles.smssync.preferences.Preferences.Keys.CONNECTED;

public class IMAPSyncPreferenceCompat extends SwitchPreferenceCompat {

    public static String[] PERMISSIONS = new String[]{Manifest.permission.INTERNET, Manifest.permission.WAKE_LOCK};

    public static boolean isEnabled(Context context) {
        final AuthPreferences auth = new AuthPreferences(context);
        return auth.getAuthMode() == AuthMode.PLAIN;
    }

    public static void onResume(PreferenceFragmentCompat f) {
        Preference p = f.findPreference(SMSApplication.PREF_IMAP);
        if (p != null)
            p.setVisible(false);
    }

    public IMAPSyncPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public IMAPSyncPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IMAPSyncPreferenceCompat(Context context) {
        super(context);
    }

    public void create() {
    }

    public void onResume() {
        CheckBoxPreference connected = (CheckBoxPreference) findPreferenceInHierarchy(CONNECTED.key);
        connected.setVisible(false);
        CheckBoxPreference firebase = (CheckBoxPreference) findPreferenceInHierarchy(SMSApplication.PREF_FIREBASE);
        firebase.setVisible(false);
        AuthPreferences auth = new AuthPreferences(getContext());
        setChecked(auth.getAuthMode() == AuthMode.PLAIN);
    }

    @Override
    public void onClick() {
        super.onClick();
    }

    public void onClickDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());

        LinearLayout title = (LinearLayout) inflater.inflate(R.layout.reboot_title, null, false);
        TextView text = (TextView) title.findViewById(R.id.item_text);
        text.setText(getTitle());
        final AuthPreferences auth = new AuthPreferences(getContext());
        ServerPreferences server = new ServerPreferences(getContext());
        final SwitchCompat sw = (SwitchCompat) title.findViewById(R.id.item_switch);
        sw.setChecked(auth.getAuthMode() == AuthMode.PLAIN);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                auth.setServerAuthMode(isChecked ? AuthType.PLAIN : AuthType.XOAUTH2);
            }
        });
        builder.setCustomTitle(title);

        View view = inflater.inflate(R.layout.imapsync, null, false);
        final EditText addr = (EditText) view.findViewById(R.id.server_address);
        addr.setText(server.getServerAddress());
        final String[] vv = getContext().getResources().getStringArray(R.array.server_protocol_entryValues);
        final Spinner ssl = (Spinner) view.findViewById(R.id.server_protocol);
        ArrayList<String> aa = new ArrayList<>(Arrays.asList(vv));
        ssl.setSelection(aa.indexOf(server.getServerProtocol()));
        final EditText login = (EditText) view.findViewById(R.id.login_user);
        login.setText(auth.getImapUsername());
        final EditText pass = (EditText) view.findViewById(R.id.login_password);
        pass.setText(auth.getImapPassword());

        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getContext());
                shared.edit().putString(ServerPreferences.SERVER_PROTOCOL, vv[ssl.getSelectedItemPosition()]).commit();
                shared.edit().putString(ServerPreferences.SERVER_ADDRESS, addr.getText().toString()).commit();
                auth.setImapUser(login.getText().toString());
                auth.setImapPassword(pass.getText().toString());
                OnBootReceiver.imap(getContext(), sw.isChecked());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

}
