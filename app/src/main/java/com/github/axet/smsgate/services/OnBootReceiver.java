package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.smsgate.dialogs.RebootDialogFragment;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.ImapSmsService;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class OnBootReceiver extends BroadcastReceiver {
    public static void start(Context context) {
        if (RebootDialogFragment.getSchedule(context).enabled && !OnExternalReceiver.mountTest(context))
            SuperUser.reboot();
        main(context);
        fragment(context);
    }

    public static void main(Context context) {
        SMSGateFragment.checkPermissions(context);
        ScheduleService.startIfEnabled(context);
        FileReplyService.startIfEnabled(context);
        if (Build.VERSION.SDK_INT >= 18)
            NotificationListener.startIfEnabled(context);
        NotificationService.startIfEnabled(context);
        FileCallsService.startIfEnabled(context);
    }

    public static void fragment(Context context) {
        SMSGateFragment.checkPermissions(context);
        FirebaseService.startIfEnabled(context);
        ImapSmsReplyService.startIfEnabled(context);
        ImapSmsService.scheduleBootupBackup(context);
    }

    public static void imap(Context context, boolean start) {
        if (start) {
            ImapSmsReplyService.startIfEnabled(context);
            ImapSmsService.startIfEnabled(context);
        } else {
            ImapSmsReplyService.stop(context);
            ImapSmsService.stop(context);
        }
    }

    public static void storage(Context context, boolean start) {
        if (start) {
            FileSmsService.startIfEnabled(context);
            FileReplyService.startIfEnabled(context);
            FileCallsService.startIfEnabled(context);
        } else {
            FileSmsService.stop(context);
            FileReplyService.stop(context);
            FileCallsService.stop(context);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOGV) Log.v(TAG, "onReceive(" + context + "," + intent + ")");
        start(context);
    }
}
