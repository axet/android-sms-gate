package com.github.axet.smsgate.services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.smsgate.BuildConfig;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.providers.SIM;
import com.zegoggles.smssync.mail.PersonLookup;

import java.util.Map;

@TargetApi(19) // SMS_DELIVER is API19+
public class IncomingPostReceiver extends BroadcastReceiver { // sms deliver handler (we have to handle storage)
    public static String TAG = IncomingPostReceiver.class.getSimpleName();

    public static int NOTIFICATION_ICON_IGNORED = 700;

    public static String SAVE = BuildConfig.APPLICATION_ID + ".services.IncomingPostReceiver.SAVE";

    public static void ignoredNotification(Context context, SmsStorage.Message m) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.ic_launcher_notification);
        builder.setTicker(context.getString(R.string.status_backup));
        builder.setWhen(System.currentTimeMillis());
        Intent intent = new Intent(SAVE);
        intent.setPackage(context.getPackageName());
        String tag = String.valueOf(System.currentTimeMillis());
        intent.putExtra("tag", tag);
        PersonLookup lookup = new PersonLookup(context.getContentResolver());
        SIM sim = SMSApplication.from(context).getSIM();
        Storage.SMSMessage msg = Storage.getMessage(sim, lookup, m);
        intent.putExtra("msg", m.save());
        builder.addAction(R.drawable.ic_baseline_save_24, "SAVE", AlarmManager.createPendingIntent(context, intent, 0));
        if (Build.VERSION.SDK_INT >= 16)
            builder.setPriority(Notification.PRIORITY_LOW);
        builder.setContentTitle("[IGNORED] " + Storage.formatName(msg));
        builder.setContentText(m.body);
        NotificationChannelCompat channel = new NotificationChannelCompat(context, "ignored", "Ignored SMS", NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(builder);
        Notification notification = builder.build();
        NotificationChannelCompat.setChannelId(notification, channel.channelId);
        NotificationManagerCompat.from(context).notify(tag, NOTIFICATION_ICON_IGNORED, notification);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Telephony.Sms.Intents.SMS_DELIVER_ACTION)) {
            SIM sim = SMSApplication.from(context).getSIM();
            Map<String, SmsStorage.Message> map = SmsStorage.getMessagesFromIntent(sim, intent);
            if (map == null)
                return;
            SmsStorage storage = new SmsStorage(context);
            for (String id : map.keySet()) {
                SmsStorage.Message m = map.get(id);
                if (Storage.filter(context, m)) { // SDK_INT < 19 handled by BackupCursors.filter()
                    Log.d(TAG, "IGNORED: " + m);
                    ignoredNotification(context, m);
                    continue;
                }
                storage.add(m);
            }
        }
        if (a.equals(SAVE)) {
            String json = intent.getStringExtra("msg");
            SmsStorage.Message msg = SmsStorage.loadMessage(json);
            SmsStorage storage = new SmsStorage(context);
            if (storage.exists(msg.remote_date, msg.phone, Telephony.Sms.DATE_SENT))
                return;
            storage.add(msg);
            IncomingPeekReceiver.incomingSMS(context, msg.remote_date);
            Toast.Text(context, "Message saved");
            String tag = intent.getStringExtra("tag");
            NotificationManagerCompat.from(context).cancel(tag, NOTIFICATION_ICON_IGNORED);
        }
    }
}
