package com.github.axet.smsgate.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.ScheduleSMS;
import com.github.axet.smsgate.dialogs.ScheduleEditDialogFragment;
import com.github.axet.smsgate.services.ScheduleService;

import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;

/**
 */
public class SchedulersFragment extends Fragment implements DialogInterface.OnDismissListener, SharedPreferences.OnSharedPreferenceChangeListener {
    Handler handler = new Handler();
    FragmentManager fm;
    boolean permittedResume = false;
    SMSApplication app;
    RecyclerView list;
    ScheduleEditDialogFragment dialog;
    Schedulers schedulers;

    public class DividerItemDecoration extends RecyclerView.ItemDecoration {
        private final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable mDivider;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            mDivider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

    public static class SchedulerViewHolder extends RecyclerView.ViewHolder {
        SwitchCompat enabled;
        TextView phone;
        TextView message;
        TextView status;

        public SchedulerViewHolder(View itemView) {
            super(itemView);
            enabled = (SwitchCompat) itemView.findViewById(R.id.schedule_item_enabled);
            phone = (TextView) itemView.findViewById(R.id.schedule_item_phone);
            message = (TextView) itemView.findViewById(R.id.schedule_item_message);
            status = (TextView) itemView.findViewById(R.id.schedule_item_status);
        }
    }

    public class Schedulers extends RecyclerView.Adapter<SchedulerViewHolder> {
        SMSApplication.ScheduleSMSStorage items;

        public Schedulers() {
            this.items = SMSApplication.from(getContext()).schedules;
        }

        @Override
        public SchedulerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.schedule_item, parent, false);
            return new SchedulerViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final SchedulerViewHolder holder, final int position) {
            final ScheduleSMS item = items.get(position);

            holder.enabled.setChecked(item.enabled);
            holder.enabled.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.enabled = holder.enabled.isChecked();
                    items.save();
                    ScheduleService.startIfEnabled(getContext());
                }
            });

            if (item.phone.isEmpty())
                holder.phone.setText(Html.fromHtml("<i>&lt;no phone&gt;</i>"));
            else
                holder.phone.setText(item.phone);

            if (item.message.isEmpty())
                holder.message.setText(Html.fromHtml("<i>&lt;no message&gt;</i>"));
            else
                holder.message.setText(item.message);

            holder.status.setText(item.formatStatus());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editDialog(items.indexOf(item));
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public void set(int pos, ScheduleSMS s) {
            if (pos == items.size() || pos == -1)
                items.add(s);
            else
                items.set(pos, s);
            notifyItemChanged(pos);
        }

        public void add(ScheduleSMS s) {
            int pos = items.size();
            items.add(s);
            notifyItemInserted(pos);
        }

        public void remove(int i) {
            items.remove(i);
            notifyItemRemoved(i);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fm = getActivity().getSupportFragmentManager();
        app = SMSApplication.from(getActivity());

        View rootView = inflater.inflate(R.layout.fragment_schedulers, container, false);

        schedulers = new Schedulers();

        list = (RecyclerView) rootView.findViewById(R.id.section_label);
        list.setAdapter(schedulers);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(mLayoutManager);
        list.setItemAnimator(new OvershootInLeftAnimator());
        list.addItemDecoration(new DividerItemDecoration(getActivity()));

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permitted())
                    editDialog(-1);
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.registerOnSharedPreferenceChangeListener(this);

        return rootView;
    }

    void editDialog(int pos) {
        if (dialog != null)
            return;

        dialog = new ScheduleEditDialogFragment();
        Bundle args = new Bundle();
        args.putInt("pos", pos);
        if (pos != -1)
            args.putString("sms", schedulers.items.get(pos).save().toString());
        dialog.setArguments(args);
        dialog.show(fm, "");
    }

    @Override
    public void onResume() {
        super.onResume();

        if (permittedResume) { // prevent crash http://stackoverflow.com/questions/37164415
            editDialog(-1);
            permittedResume = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (permitted(permissions))
                    permittedResume = true;
                else
                    Toast.makeText(getActivity(), R.string.not_permitted, Toast.LENGTH_SHORT).show();
        }
    }

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS};

    boolean permitted(String[] ss) {
        for (String s : ss) {
            if (ContextCompat.checkSelfPermission(getActivity(), s) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    boolean permitted() {
        for (String s : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getActivity(), s) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(PERMISSIONS, 1);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (this.dialog == null)
            return;
        if (dialog instanceof ScheduleEditDialogFragment.ScheduleResult) {
            ScheduleEditDialogFragment.ScheduleResult r = (ScheduleEditDialogFragment.ScheduleResult) dialog;
            if (r.delete) {
                schedulers.remove(r.pos);
                schedulers.items.save();
            }
            if (r.save) {
                if (r.pos == -1)
                    schedulers.add(r.schedule);
                else
                    schedulers.set(r.pos, r.schedule);
                schedulers.items.save();
            }
            ScheduleService.startIfEnabled(getContext());
        }
        this.dialog = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.startsWith(SMSApplication.SCHEDULER_ITEM))
            schedulers.notifyDataSetChanged();
    }
}
