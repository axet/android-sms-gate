package com.github.axet.smsgate.widgets;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.widget.SwitchCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.axet.androidlibrary.preferences.TTSPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.NotificationListener;
import com.github.axet.smsgate.services.NotificationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class NotificationsPreference extends SwitchPreferenceCompat {

    public static String SYSTEMUI = "com.android.systemui";
    public static String PHONE = "com.android.phone";

    List<ApplicationInfo> packages;
    Set<String> set;
    Button neutral;
    AlertDialog d;

    public static boolean isEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= 18)
            return NotificationListener.enabled(context);
        else
            return NotificationService.enabled(context);
    }

    public static Set<String> load(String key) {
        if (key == null)
            return new TreeSet<>();
        String[] ss = key.split("\n");
        Set<String> set = new TreeSet<>();
        for (String s : ss)
            set.add(s.trim());
        set.remove("");
        return set;
    }

    public static boolean contains(Set<String> ss, String key) {
        for (String s : ss) {
            if (s.startsWith(key))
                return true;
        }
        return false;
    }

    public class ApplicationsAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return packages.size();
        }

        @Override
        public Object getItem(int position) {
            return packages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.application_item, parent, false);
            }
            final ApplicationInfo info = packages.get(position);
            PackageManager pm = getContext().getPackageManager();
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            TextView text = (TextView) convertView.findViewById(R.id.item_text);
            TextView sum = (TextView) convertView.findViewById(R.id.item_summary);
            final SwitchCompat sw = (SwitchCompat) convertView.findViewById(R.id.item_switch);

            String n = FirebaseService.getApplicationName(getContext(), info);
            Drawable d = info.loadIcon(pm);

            icon.setImageDrawable(d);
            text.setText(n);
            sum.setText(info.packageName);
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setChecked(info, !sw.isChecked());
                }
            });
            sw.setOnCheckedChangeListener(null);
            sw.setChecked(isChecked(info));
            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    setChecked(info, isChecked);
                }
            });
            return convertView;
        }
    }

    @TargetApi(21)
    public NotificationsPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public NotificationsPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public NotificationsPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public NotificationsPreference(Context context) {
        super(context);
        create();
    }

    public void setText(String text) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(getKey(), text);
        SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
        set = load(text);
    }

    public String getText() {
        SharedPreferences shared = getSharedPreferences();
        return shared.getString(getKey(), null);
    }

    void create() {
        final PackageManager pm = getContext().getPackageManager();
        // packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        Set<String> ss = new TreeSet<>();
        packages = new ArrayList<>();
        try {
            packages.add(pm.getApplicationInfo(PHONE, 0));
        } catch (PackageManager.NameNotFoundException e) {
        }
        try {
            packages.add(pm.getApplicationInfo(SYSTEMUI, 0));
        } catch (PackageManager.NameNotFoundException e) {
        }
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pkgAppsList = pm.queryIntentActivities(mainIntent, 0);
        if (pkgAppsList == null)
            return; // Android Studio editor
        for (ResolveInfo info : pkgAppsList) {
            ApplicationInfo i = info.activityInfo.applicationInfo;
            if (!ss.contains(i.packageName)) {
                ss.add(i.packageName);
                packages.add(i);
            }
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onClick() {
        if (d != null)
            return;
        set = load(getText());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setAdapter(new ApplicationsAdapter(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ;
            }
        });
        builder.setNeutralButton("Disabled", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setText(save(set));
                if (!set.isEmpty())
                    callChangeListener(set);
            }
        });
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                d = null;
            }
        });
        d = builder.create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                neutral = d.getButton(AlertDialog.BUTTON_NEUTRAL);
                updateNeutral();
            }
        });
        Window w = d.getWindow();
        final Window.Callback c = w.getCallback();
        w.setCallback(new WindowCallbackWrapper(c) {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                c.onWindowFocusChanged(hasFocus);
                updateNeutral();
            }
        });
        d.show();
    }

    void updateNeutral() {
        if (neutral == null)
            return;
        neutral.setText(TTSPreferenceCompat.getImageText(getContext(), R.drawable.baseline_open_in_new_24, R.attr.colorAccent));
        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 18)
                    NotificationListener.show(getContext());
                else
                    NotificationService.show(getContext());
                updateNeutral();
            }
        });
    }

    boolean isChecked(ApplicationInfo info) {
        return contains(set, info.packageName);
    }

    String save(Set<String> set) {
        String text = "";
        for (String s : set)
            text += s + "\n";
        return text;
    }

    void setChecked(ApplicationInfo info, boolean b) {
        if (b)
            set.add(info.packageName);
        else
            set.remove(info.packageName);
    }

    public void onResume() {
        setChecked(isEnabled(getContext()));
    }
}
