package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class OnUpgradeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        OnBootReceiver.start(context);
        if (Build.VERSION.SDK_INT >= 18)
            NotificationListener.requestRebind(context);
    }
}
